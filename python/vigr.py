#
# VIgR main package
#
# (c) Charles Bird, 2016
#

import os
import sys
import logging
import logging.config
import configparser
import argparse
import webbrowser

# local package dependencies

import vigr_globals as globals
import vigr_config as vconfig
import vigr_utils as vutils
import vigr_exceptions as vx

def read_default_config():
    default_cfg_fname = os.path.join( vutils.find_global_defaults_dir(), globals.default_cfg_fname)
    config = configparser.ConfigParser()

    ### need to add something about package integrity here
    ### code here or list from manifest??

    ## this is just checking the file exists, not that it's parsable
    try:
        with open(default_cfg_fname) as test_f:
            log_str = " ".join(["VIgR - opened ", str(default_cfg_fname), " successfully"])
            ### wrong logging level until sort out logging setup
            logging.warning(log_str)
    except IOError as e:
            log_str = " ".join(["Failed to open ", str(default_cfg_fname)])
            logging.error( log_str)
            raise vx.configOpenError(default_cfg_fname)


    try:
        config.loadConfigObject(config, default_cfg_fname)
    except Exception as e:
        print (e)
        log_str = "".join(["Unable to read global defaults from [",
                            default_cfg_fname,
                            "] - reverting to hard coded defaults" ])
        logging.warning(log_str)
        raise vx.configParseError(default_cfg_fname)
    ## add call to config object to dict functions - use flag?? or just none as marker
    ## this "hard" options could be overridden by the command line, so we need to do this here
   

   

def build_cmd_line_parser(cfg_obj):                    
    
    
    try:
        desc_str = "Starts local VIgR (Visualing Immunoglobulin Repertoires) api server"
                # "".join(['Starts local VIgR (Visualing Immunoglobulin Repertoires) api server',
                ##"(defaults to ",
                ##str(globals.default_api_addr),":",str(globals.default_api_port),")"
                ##])

        ## run a check on currency and deploy update message if appropriate
        ## can do some stuff with local cookies here as well to track usage
        
        ver = vutils.vigr_version()
        prg_name = vutils.get_clean_prog_name()
        ver_str = "".join([ prg_name, " v", str(ver) ])

        # make this an async call to defer on internet connection
        
        print ("Please wait a few seconds while ",prg_name, " checks if an update is available ...")
        if ver.isCurrentVersion():
            logging.debug("Using current version %s", str(ver) )
            parser = argparse.ArgumentParser(description = desc_str)    # idc use dict to support update check
            ver_str = "".join([ vutils.get_clean_prog_name()," v", str(ver) ])
        else:
            logging.debug("Current version %s is out of data", str(ver))
            update_type = ver.fetch_update_type()
            ver_str = "".join([ vutils.get_clean_prog_name()," v", str(ver), " ** Update available" ])
            epilog_str = "".join(["A ",update_type, " update is available from ", globals.update_src])
            parser = argparse.ArgumentParser(description = desc_str, epilog = epilog_str)
            
            # ## or run --update
            # ## parser.add_argument('--update', action='update')

        parser.add_argument('-v','--version', action='version', version=ver_str)
         
        # run over the command line options in cfg_obj
        # first check and extract options and help
         
        if globals.cmd_ln_sect in cfg_obj.list_config_sections():
            logging.debug("building command line parser from configuration section %s",globals.cmd_ln_sect)
            cmd_opts = cfg_obj.map_config_section(globals.cmd_ln_sect)
            if globals.cmd_ln_help in cfg_obj.list_config_sections():
                cmd_help = cfg_obj.map_config_section(globals.cmd_ln_help)
            else:
                cmd_help = None
            
            if globals.run_debug_code:
                logging.debug("found the following options/values:")
                logging.debug(cmd_opts)
                logging.debug("found the following help:")
                logging.debug(cmd_help)
                
        else:
            cmd_opts = None
            cmd_help = None
            logging.warning("Expected section [%s] missing from default configuration file",globals.cmd_ln_sect)
                  
        ### now add parse arguments
        
        # first build combined list from union of hardcoded options and config file
        if cmd_opts:
            all_opts = set.union(set(cmd_opts), set(cfg_obj._config))
        else:
            all_opts = cfg_obj._config
            
        for opt in all_opts:
            flag = "".join(["--", opt]) ## no abbreviated flags yet, and type checking not possible with current dict TODO
  
            # extract default value, first from config file then anything hardcoded
            if cmd_opts and cmd_opts[opt]:  ## rely on short circuit evaluation to skip None object
                    def_val = cmd_opts[opt]
            elif cfg_obj._config[opt]:
                def_val = cfg_obj._config[opt]
            else:
                def_val = None
            
            # extract help string, and if default exists append
            if cmd_help and (opt in cmd_help):  ## rely on short circuit evaluation to skip None object
                help_str = cmd_help[opt]
                if def_val:
                    help_str = "".join([ help_str," [",str(def_val),"]"])
            else:
                help_str = None            
               
            parser.add_argument( flag, default = def_val, help = help_str )                        
        return parser
    except:
        raise

def configure_logging(logging_cfg_fname):
    try:
        with open(logging_cfg_fname) as f:
            log_str = " ".join(["VIgR - opened ", str(logging_cfg_fname), " successfully"])
            logging.debug(log_str)
        ## should we drop back from context manager to avoid lock??
            logging.config.fileConfig(logging_cfg_fname)
        ## need to catch errors here
            logger = logging.getLogger(globals.root_logger_name)
            logger.debug("created logger from config file successfully")
        return logger
    except IOError as e:
        log_str = " ".join(["VIgR - Failed to open ", str(logging_cfg_fname)])
        ## NB use the basic logging stream here
        logging.error(log_str)
        raise vx.configOpenError(logging_cfg_fname)
    except:
        logging.error("Parsing error in configure logging")
        raise
    ## catch what ever logging.config barfs on bad file ******
        
        
def main():

    ## so what we need to do here is 
    # 1) setup an initial verbose logger to handle early errors associated
    #       with a corrupt install
    # 2) read the global config from the install file
    # 3) use these values to configure command line parser, incl defaults
    # 4) process command line to update operating values
    # 5) start local api server if selected, and spawn browser pointing at defined location
    
    # need to think about these should/could interact with per user settings    

    ## put a basic logger in place, which we'll stop using/discard once a properly
    ## configured system is in place, which depends on running config and parser
    ## in case a different logging setup is specified
    
    print ("VIgR starting ...")
    
    logging.basicConfig(level=globals.preconfig_logging_level)
    logging.debug("VIgR - Basic preconfiguration logger running")

    ## read defaults file into configuration object
    ### what to do here?
    vigr_cfg = vconfig.config_obj()
    default_cfg_fname = vutils.fully_qualify_default_path_to( globals.default_cfg_fname )
    vigr_cfg.load_config_file(default_cfg_fname)
    
    ## build parser, taking defaults from configuration object
    parser = build_cmd_line_parser(vigr_cfg)
       
    args = parser.parse_args() 
    logging.debug("Parsed values from command line follow: ")
    logging.debug("======================================= ")
    logging.debug(args)

    # update program configuration
    vigr_cfg.update_config( vars(args) )    # passes args namespace as dict
    
    ## build final logging configuration depending on command line
    ## have to pass fully qualified location
    
    ## TODO will eventually pick these values from vigr_cfg
    logging_cfg_fname = vutils.fully_qualify_default_path_to( globals.logging_cfg_fname )
    
    logger = configure_logging(logging_cfg_fname)
    
    # from here on, use (lowercase) loggER.xxx not loggING.xxx
    logger.debug("Completed configured logger setup in main")
    
    ## now we have a configured and logged baseline to initiate
    ## API and backend services from
    ## ########################################################
    
    # both of these might block on GIL under IDLE 
    # will need to look into threads etc
    #
    # start local api server
    
    #print ("Starting api server at ", vigr_cfg.fetch_value_for("host"),
    #        ":", vigr_cfg.fetch_value_for("port"))
    
    # spawn browser pointing at defined location
    ############################################
    
    #tgt_url = "".join([ "http://", vigr_cfg.fetch_value_for("host"), 
    #                    ":", vigr_cfg.fetch_value_for("port"),
    #                    "index.html"])
    #webbrowser.open_new(tgt_url)
    
    ## exit events
    logging.shutdown()
    

if __name__ == "__main__":
    main()
    



