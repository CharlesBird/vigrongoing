#! /usr/bin/python
#
# (c) Charles Bird, 2016
#
# manage settings (eg libraries)
# initially start with local file resources, but this will also handle cloud
# data locations (to pass to api backend - not to execute analysis in this module)
#

import logging

# AWS
import boto3
 
# local dependencies
import vigr_utils as vutils

mod_logger=logging.getLogger(__name__)

# local dependencies
import vigr_globals as globals

class user_settings():
    
    // will need to handle some authorisation elements here

    def __init__():
        pass
    
    // need receive and transmit functions
    // save to persistent resources
    //      friends/overloads??
    
    def set_default_settings_location():
        pass
    
    
    def save_obj_to_file(fname):
        pass
        
    // api function
    def load_obj_from_file(fname):
        with open(fname) as f:
            
        pass
    
    def check_permissions_settings_location():
        pass
    

    
def main():
    print ("Running ", vutils.get_clean_prog_name(), " as main")
    
    
if __name__ == "__main__":
    logging.basicConfig(level=globals.preconfig_logging_level)
    mod_logger = logging.getLogger(__name__)
    mod_logger_console= logging.StreamHandler(sys.stdout)
    mod_logger_console.setLevel(logging.DEBUG)
    main()