#!/usr/bin/python3
#
# 5 Apr 2016, Charles Bird
#
''' diversity measures '''
''' Hill-type indicies '''
''' see Hill 1973, Jost 2006, Grieff 2015 '''

# Hill MO. (1973). Diversity and evenness: a unifying notation and its consequences. 
# Ecology, 54(2), 427-432. http://www.jstor.org/stable/1934352

# Jost L. (2006). Entropy and diversity. Oikos, 113(2), 363–375. 
# doi:10.1111/j.2006.0030-1299.14714.x

# Greiff V, Bhat P, Cook SC, Menzel U, Kang W, & Reddy ST. (2015). A bioinformatic 
# framework for immune repertoire diversity profiling enables detection of 
# immunological status. Genome Medicine, 7(1). doi:10.1186/s13073-015-0169-8

# Horn HS. (1966) Measurement of 'overlap' in comparative ecological studies.
# Am. Nat. 100, 419-424


#### musings in development
############
# right - step back from the details of how you get them and focus on 
# the essential considerations - you need to describe a sample which has
# an arbitrary sized set of identifiers (eg species, clonotype), each of
# which has either an explicit "number of members" or an implicit size
# derived from a membership list/array etc (note this could be recursive)
#
# hence the essential issues are
# - build a representation as above
# - calculate indicies as per references
#

### (skbio.math.diversity.alpha) provides many of the named measures
### but can only handle an array of counts, and doesn't provide
### the ability to tune the "q" order parameter for the diversity as defined in
### Jost, 2006
### That ability to generate diversity values across a range of "q" to derive
### a diversity profile as per Grieff is the essential element in
### overcoming the challenges of power law distributions across different
### populations



import math as Math
import json

from enum import Enum
class ObjContains(Enum):
    COUNTS      = 1,
    PROPORTIONS = 2,
    MEMBERS     = 3

    
# should be in a utility module, but this
# seems to be accepted as "pythonic" solution
def isNumber(s):
    try:
        float(s)
    except ValueError:
        return False
    except TypeError:
        return False
    else:
        return True

##################### json stuff (separate module idc)        

def json_serialize_obj( obj ):

    # Convert objects to a dictionary of their representation
    d = { '__class__':obj.__class__.__name__, 
          '__module__':obj.__module__,
          }
    d.update(obj.__dict__)
    return d

def convert_json_to_obj(d):
    if '__class__' in d:
        class_name = d.pop('__class__')
        module_name = d.pop('__module__')
        module = __import__(module_name)
        
        class_ = getattr(module, class_name)
        
        args = dict( (key.encode('ascii'), value) for key, value in d.items())
        
        inst = class_(**args)
    else:
        inst = d
    return inst

    

class adapterObj:

# idc escape strings

    def __init__ (self, description, adapter_string ):
        self._obj = dict()
        self._obj['desc'] = description
        self._obj['string'] = adapter_string
        
class vigrMsg:

# IDC escape strings

    def __init__(self, type_str, description_str):
        self._json = dict()
        self._adapters = list()
        self._json['type'] = type_str
        self._json['desc'] = description_str
    
    #def add_adapter( self, adapterObj ):
     #   self._adapters.append( adapterObj )     #### HERE WITH STRINGIFY
     
    def add_adapter( self, adapter_desc, adapter_dict, label_dict = {} ):
        adapter = dict()
        adapter["desc"] = adapter_desc
        adapter["adpt_dict"] = adapter_dict
        adapter["labels"] = label_dict
        self._adapters.append( adapter )     #### HERE WITH STRINGIFY
        
    def add_data_provides ( self, arr_basic_data_types ):
        ''' provide info on what graph requirements the data can provide '''
        self._json['provides'] = arr_basic_data_types
        
    def add_data_array (self, data_arr):    ## should wrap with more class aware function
        self._json['data'] = data_arr

    # utility function - not intended for routine use
    ## this won't handle a user-defined object TODO
    def add_entity (self, key_str, contents):    
        if key_str in self._json:
            raise ValueError("vigrMsg.add_entity attemping to overwrite existing key ", key_str)
        self._json[key_str]=contents
        
    # can add json.encoder options for streams over large datasets
    
    def get_json( self ):
        self._json['adapters'] = self._adapters
        # return( json.dumps ( self._json, default = json_serialize_obj ) )
        return( json.dumps ( self._json ) )
              
##### think of a better name for this

class Population:
## what we should do here is take the presented object
## and generate the requisite description ( p_i = n )
## so you can then call diversity (q) on an object of this class
##
## idc should look at moving towards a generator idiom to
## improve performance with large populations
##
  
## ideally we could just call the constructor with the object, but
## there's no way to intelligently decide what (count, proportion, id) 
## is _in_ the passed object without iterating over object, so
## we need to pass an identifiers
##
    def __init__( self, obj_handle, obj_contains_t, *, renormalise = True ):
        self._obj_t = obj_contains_t    ## need to track this for comparative measures taking two ids
                                        ## such as Moritsa-Horn overlap index
        self._pop = dict()
        self._prop_i = dict()   ## proportion
      
        if ObjContains.COUNTS == obj_contains_t:
            self.analyse_count_obj(obj_handle)
        elif ObjContains.PROPORTIONS == obj_contains_t:
            self.analyse_proportion_obj(obj_handle, renormalise )
        elif ObjContains.MEMBERS == obj_contains_t:
            self.analyse_member_obj(obj_handle)
        else:
            raise ValueError("can't handle objects of type ", obj_contains_t)

       
    def analyse_count_obj( self, obj_handle ):     
    ## is a check on isInt(count) sensible?
    ## does itertools retain performance advantage? 
        running_total = 0;
        
        if ( type( dict() ) == type(obj_handle) ):
            for k, v in obj_handle.items():
                self._pop[k] = v
                running_total += v
        elif ( type( list() ) == type(obj_handle) ):
            for i, v in enumerate(obj_handle):
                self._pop[i]= v
                running_total += v
        else:
                raise TypeError("can't handle supplied type: ", type(obj_handle))   ## idc better error
 
                
        if 0 > running_total:
            raise ValueError("running total on analyse_obj_count is negative")
        if 0 == running_total:
            for k, v in self._pop.items():
                self._prop_i[k] =  0
        else:
            for k, v in self._pop.items():
                self._prop_i[k] = self._pop[k] / running_total
    
    def analyse_proportion_obj( self, obj_handle, renormalise ): 
    # if the set total is <> 1.0, then if renormalise is true, set values
    # will be adjusted to fix (there are some interesting sets in published
    # papers where this is not true and the resulting diversities are wrong)
    #
        running_total = 0
    
        if ( type( dict() ) == type(obj_handle) ):
            for k, v in obj_handle.items():    
                self._pop[k]= v
                running_total += v
        elif ( type( list() ) == type(obj_handle) ):
            for i, v in enumerate(obj_handle):
                self._pop[i]= v
                if isNumber(v):
                    running_total += v
        else:
            raise TypeError("can't handle supplied type: ", type(obj_handle) )   ## idc better error
        
        if renormalise and ( 1 != running_total ):
        ## comments are test code
        #    runtot2 = 0
            for i, v in enumerate(self._pop):
                self._pop[v] /= running_total
        #        runtot2 += self._pop[v]
        #    print (":: ", runtot2)
                
    
    
    def analyse_member_obj( self, obj_handle ):
        ''' increments count on each appearance of id '''
    
        running_total = 0
            
            ## need to be cleverer on contents of dict value
        if ( type( dict() ) == type( obj_handle ) ):
            for k, v in obj_handle.items():
                if isNumber(v):
                    if k in self._pop:
                        self._pop[k] += v
                    else:
                        self._pop[k] = v
                    running_total += v
                elif type(list()) == type (v):
                    if k in self._pop:
                        self._pop[k] += len(v)
                    else:
                        self._pop[k] = len(v)
                    running_total += len(v)
        elif ( type( list() ) == type(obj_handle) ): 
            for i, v in enumerate(obj_handle):
                if v in self._pop:
                    self._pop[v] += 1
                else:
                    self._pop[v] = 1
                running_total += 1
        else:
            raise TypeError("can't handle supplied type: ", type(obj_handle))   ## idc better error
                
        if 0 > running_total:
            raise ValueError("running total on analyse_obj_count is negative")
        if 0 == running_total:
            for k, v in self._pop.items():
                self._prop_i[k] =  0
        else:
            for k, v in self._pop.items():
                self._prop_i[k] = self._pop[k] / running_total
    
    def length(self):
        return len(self._pop)
    
    def div_index( self, order ):
        ''' calculate (q_D)i as defined in Jost, 2006 '''
        ''' doi:10.1111/j.2006.0030-1299.14714.x '''
        ''' where q is represnted by "order" '''
        
        running_total = 0
              
        if ( 1 == order ):  ## this is the shannon case, and only exists as lim q->1
            for k, v in self._prop_i.items():
                running_total += v * Math.log (v) 
            return ( Math.exp ( -1 * running_total ) )
        else:   ## could short cut order==0 case, but unlikely to be major timing constraint
            for k, v in self._prop_i.items():
                running_total += pow ( v, order )
            return pow ( running_total, ( 1 / ( 1 - order) ) )


    
    def diversity_profile ( self, order_start, order_end, step = 1.0 ):
        ''' returns a list of tuples (order, order_D) '''   
        ## has to be list - order could be -ve or non-integer
        ## note expect order and step to be +ve monotonic
        
        if order_end <= order_start:
            raise ValueError("Diversity profile called with end <= start")
        if 0 >= step:    
            raise ValueError("Diversity profile called with step <= 0")        
        
        tmp = list()
        order = order_start
        while ( order < order_end ):
            tmp.append( (order, self.div_index(order) ))
            order += step
        return tmp
        
    ## could named values here, either directly or pass
    ## _pop array to skbio functions    
    
    # def shannon(self):
        # pass

    # def gini(self):
        # pass

    # def simpson(self):
        # pass

    # def richness(self):
        # pass
        
    def MorisitaHorn (self, pop2):
        ''' Morisita-Horn Index between self object and pop(ulation)2 object '''
        
        # build overall union of species in both populations
        
        set_x = set(self._pop.keys())
        set_y = set(pop2._pop.keys())
        union = set_x | set_y

        if 0 == len(set_x) or 0 == len(set_y):
            raise ValueError("Attempting to calculate Morisita-Horn index with an empty population")
        
        # iterate over joint list of species and calculate partial functions
        numerator = 0
        denom_x2  = 0
        denom_y2  = 0
        X = 0
        Y = 0
        for s in union:
            if (s in set_x) and (s in set_y):
                numerator += self._pop[s] * pop2._pop[s]
            if (s in set_x):
                denom_x2 += (self._pop[s] * self._pop[s])
                X += self._pop[s]
            if (s in set_y):
                denom_y2 += (pop2._pop[s] * pop2._pop[s])
                Y += pop2._pop[s]
        
        # add numerical factors and combine denominator elements`
        numerator *= 2    
        denom_x2 /= (X * X)
        denom_y2 /= (Y * Y)
        
        denom = (denom_x2 + denom_y2 ) * X * Y
            
        return ( numerator / denom )

        
    def vigr_div_profile( self, order_start, order_end, step, desc = "" ):
        ''' package a profile into json object with adapter(s) and data '''
        
        msg = vigrMsg ("diversity_profile", desc)
        
        msg.add_adapter("x=order y=D(order)", '{x:"d[0]",y:"d[1]"}', {"x":"order", "y":"Div(order)"} )
        msg.add_data_provides(["x","y"])    # should probably be even more basic
                                            # for max flexibility but will work for now
        msg.add_data_array ( self.diversity_profile( order_start, order_end, step) )
        
        return ( msg.get_json() )
      
############ end class Population  ##########


## next metric to do??
class network_density:

    def __init__( self ):
        pass


############ vigr_msg  ##########

## standalone json parser function
def parse_vigr_package ( raw_json_str ):
    # testing
    print ("\n========= now parse back \n")
    #check = json.loads( raw_json_str,  object_hook= convert_json_to_obj )
    check = json.loads( raw_json_str )
    print (repr(check))
    print ("Type->", check['type'])
    print ("Desc->", check['desc'])
    print ("Adapters->", check['adapters'])
    adapters = check['adapters']
    for a in adapters:
    
        print (a)
        print (a["labels"]["x"])
       
    print ("Data->", check['data'])
    data = check['data']
    for d in data:
        print (d," = ",d[0],":",d[1])
   
def main():

    listCnt = [ 3,5,4,3,8 ] # effectively 1D array
    uniform = [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]
    uni_long = [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]
    uniform10 = [ 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 ]
    spike = [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 10 ]
    dictCnt = {'a':3,'b':5,'c':4,'d':3,'e':8}
    dict2Cnt = {'a':5,'b':8,'c':1,'f':3,'g':8}  # incomplete overlap
    dict3Cnt = {'a':3,'b':5,'c':4,'d':3,'e':8}  # complete MH = 1
    dict4Cnt = {'m':3,'n':5,'o':4,'p':3,'q':8}  # no overlap MH = 0

    dictLevel = {'a':3,'b':3,'c':3,'d':3,'e':3}

    prop = (0.2,0.2,0.1,0,0.1,0.5)
   
    print ("Testing popn_diversity class")
    
    pop = Population(listCnt, ObjContains.COUNTS )
    uni = Population(uniform, ObjContains.COUNTS )
    uni10 = Population(uniform10, ObjContains.COUNTS )
    unilong = Population(uni_long, ObjContains.COUNTS )
    spk = Population(spike, ObjContains.COUNTS )
    
    print ("-------------")
    
    ## need to test prop object again
    
    if 1==0:
        q = -1.0
        for i in ( range( 0, 2 )):
            print ("ListCnt Diversity Index D(", q,") = ", pop.div_index( q ) )
            print ("Uniform Diversity Index D(", q,") = ", uni.div_index( q ) )
            print ("Uniform10 Diversity Index D(", q,") = ", uni10.div_index( q ) )
            print ("UniformLong Diversity Index D(", q,") = ", unilong.div_index( q ) )
            print ("Spike Diversity Index D(", q,") = ", spk.div_index( q ) )
            q += 0.5
        
    print ("Profile: ",spk.diversity_profile(-1, 5, 0.5) )
    print ("==============")
  
    json_str = spk.vigr_div_profile( -1, 5, 0.5, "spiked population" )
    parse_vigr_package (json_str)
  
    if 1==0:   
        print ()
        print ("Member values")
        dct = Population( dictCnt, ObjContains.MEMBERS )
        dct2 = Population( dict2Cnt, ObjContains.MEMBERS )  # 0 < MH < 1 with dct
        dct3 = Population( dict3Cnt, ObjContains.MEMBERS )  # MH 1 with dct
        dct4 = Population( dict4Cnt, ObjContains.MEMBERS )  # MH 0 with dct
        
        q = -0.5
        for i in ( range( 0, 5 ) ):
            print ("dictCnt Diversity Index D(", q,") = ", dct.div_index( q ) )
            q += 0.5
        
        print ("Morisita-Horn index")
        print ("Complete overlap: ",dct.MorisitaHorn(dct3) )
        print ("No overlap: ",dct.MorisitaHorn(dct4) )
        print ("Partial overlap: ",dct.MorisitaHorn(dct2) )
            
   
    
        
if __name__ == '__main__':
    main()

        
        
        