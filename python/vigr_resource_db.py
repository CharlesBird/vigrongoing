#! /usr/bin/python

import sqlite3 as sql
import logging
import re
import sys

## local modules
import vigr_globals as globals

mod_logger = logging.getLogger(__name__)


## we'll hold the db schema in json format using standard SQL to make config earlier
## eventually put these into a class for overloading by backend type

admin_db_tables = {
        #"user":"CREATE TABLE users(id INTEGER PRIMARY KEY, email TEXT unique) IF NOT EXISTS"
        "user":"CREATE TABLE users(id INTEGER PRIMARY KEY, email TEXT unique)"
        }
        
test_users = [
        ("bill.gates@microsoft.com",),
        ("mark.zuckerberg@facebook.com",)
        ]

## create a transient DB for initial development
## should wrap db open/close behaviour in a context manager
## and abstract query behaviours for portability

def create_tables(db_conn, table_dict, *, check_create=True):
    try:
        assert(db_conn)
        cursor = db_conn.cursor()
        for tbl in table_dict:
            sql_cmd = table_dict[tbl]
            print (table_dict[tbl])
            if check_create:
                create_pattern="^CREATE TABLE .*"
                if re.match(create_pattern, sql_cmd):
                    cursor.execute(sql_cmd)
                else:
                    raise ValueError("command rejected: ", str(sql_cmd))
            else:
                cursor.execute(sql_cmd)
        db_conn.commit()
    except ValueError as e:
       mod_logger.error("create tables called with dict contain other SQL commands - rolling back")
       db_conn.rollback()
       raise

def load_user_table(db_conn, user_list):
    try:
        cursor = db_conn.cursor()
        sql_cmd = "INSERT INTO users (email) VALUES(?)"
        for u in user_list:
            print (u)
        cursor.executemany(sql_cmd, user_list )
    except sql.IntegrityError as e:
        mod_logger.error("load_user_table: integrity violation - rollback")
        raise
    except:
        raise
        
def show_user_table(db_conn):
    try:
        cursor = db_conn.cursor()
        sql_cmd = "SELECT * FROM users"
        cursor.execute(sql_cmd)
        all_rows = cursor.fetchall()
        if globals.run_debug_code:
            mod_logger.debug("Printing users table")
            for r in all_rows:
                mod_logger.debug(r)
        return all_rows
    except:
        raise
    
       
def list_columns(db_conn, table_name):  ## just show SQL CREATE for now, but ok for debugging
    try:
        cursor = db_conn.cursor()
        sql_cmd = "SELECT sql FROM sqlite_master WHERE tbl_name = ? AND type = 'table' "
        cursor.execute(sql_cmd, list(table_name) )
        all_cols = cursor.fetchall()
        if globals.run_debug_code:
            for col in all_cols:
                mod_logger.debug(col)
        return all_cols
    except:
        raise
   
def list_tables(db_conn):
    try:
        cursor = db_conn.cursor()
        sql_cmd = " SELECT name FROM sqlite_master WHERE type='table' "
        cursor.execute(sql_cmd)
        all_rows = cursor.fetchall()
        if globals.run_debug_code:
            for row in all_rows:
                mod_logger.debug(row)
                list_columns(db_conn, row)
        return all_rows
    except:
        raise
        
def main():
    print ("running as main")
    ## bring up independent simple logger
    logging.basicConfig(level=globals.preconfig_logging_level)
    mod_logger = logging.getLogger(__name__)
    mod_logger_console= logging.StreamHandler(sys.stdout)
    mod_logger_console.setLevel(logging.DEBUG)
    with sql.connect(":memory:") as admin_db:
        print ("creating tables")
        create_tables(admin_db, admin_db_tables)
        print ("listing tables & columns")
        list_tables(admin_db)
        print ("loading user table")
        load_user_table(admin_db, test_users)
        show_user_table(admin_db)

        
if __name__ == "__main__":
    main()



