#! /usr/bin/python
#
# (c) Charles Bird, 2016
#
# various service utilities
#

import sys
import os
import json
import requests
import logging
import re
import glob
import pprint
from enum import IntEnum, unique   # python > 3.4

import vigr_globals as globals

@unique
class API_STAGE(IntEnum): 
    ALPHA      = 0
    BETA       = 1
    PRODUCTION = 2
    
api_stage_folder = ["alpha","beta","prod"]
api_stage_desc = ["alpha","beta","production"]  ## ideally refer to these in version deployment code

mod_logger = logging.getLogger(__name__)

class vigr_version():
    # _version = {}
    # _stage = -1
    # _remote_ver = None
        
    def __init__(self):
        self._stage   = API_STAGE.BETA
        self._version = {   "type" :api_stage_desc[self._stage],
                            "major":0,
                            "minor":0,
                            "revision":1  ## must be integer, no 1a's
                        }     
                                    ## ideally need to get this to track pull from git
                                    ## possibly via clean/smudge trick
                                    ## also need to fetch the type etc
                                    ## probably a small json in branch
    
    def __str__(self):
        return '%d.%d.%d (%s)' % (  self._version["major"],
                                    self._version["minor"],
                                    self._version["revision"], 
                                    self._version["type"])
        
    def this_version(self):
        return (self._version)
        
    def isCurrentVersion(self):         ## could return as list (T/F, maj/min/rev)
        try:    
            ver_json = self.fetch_cur_vers_from_api_server()    ## this is None if can't fetch from server
            if ver_json:
                self._remote_ver = json.loads(ver_json)
            ### need to think about how to serve this object - is a monolithic object most efficient
            ### or should we break by type
                for v in self._remote_ver:
                    if (v["type"].lower() == self._version["type"].lower() ):
                        cur_major = int(v["major"])
                        cur_minor = int(v["minor"])
                        cur_rev   = int(v["revision"])
                        mod_logger.debug("Running version: %d.%d.%d", 
                                            self._version["major"],self._version["minor"],self._version["revision"])
                        mod_logger.debug("Latest version: %d.%d.%d", 
                                            cur_major, cur_minor, cur_rev)
                        if cur_major > self._version["major"]:
                            return False
                        elif cur_minor > self._version["minor"]:
                            return False
                        elif cur_rev > self._version["revision"]:
                            return False
                        else:
                            return True
            else:
                return True     # skip over an inability to check, in anticipation of later check succeeding`
        except Exception as e:
            mod_logger.error("*****************************************************************************")
            mod_logger.error("**** isCurrentVersion couldn't parse version object correctly - skipped check")
            mod_logger.error(e)
            return True
            
    def fetch_update_type(self):    ## TODO`
        ## compare major/minor and criticality, and return sensible string
        ## probably as a tuple      
        return ""         ## needs fix - should return some measure of criticality
        
    def fetch_update_changelog():   ## TODO
        return ""
       
    def fetch_cur_vers_from_api_server(self):   
        try:
            if self._stage not in API_STAGE:
                raise ValueError("vigr_version object has unknown value for _stage")
            
            api_url = "".join([ globals.public_api_url,
                                api_stage_folder[self._stage],"/",
                                globals.public_version_api ])
            r = requests.get(api_url)
            if (r.status_code != requests.codes.ok):
                mod_logger.info("unable to query current version from server = skipping check")
                return None
            elif not are_headers_correct(globals.json_valid_hdrs, r ):
                mod_logger.info("current version object from server has incorrect headers - skipping check")
                return None
            else:
                return r.text
        except requests.RequestException as e:
            
            mod_logger.info("**** skipping vigr_version check: couldn\'t get object from server: ")
            mod_logger.info(e)
            return None;   
        except ValueError as e:
            mod_logger.error("******************************************************")
            mod_logger.error("****  vigr_version object has unknown value for _stage")
            mod_logger.error("******************************************************")
            raise
        
       
## returned web data validation tools
#####################################

def are_headers_correct(validation_dict, requests_obj):  ## this approach mandates hdr to appear in returned object
    areCorrectFlag = True
    for hdr in validation_dict:
        match_pattern = "".join([ ".*", validation_dict[hdr], ".*" ])
        returned_hdr = requests_obj.headers.get(hdr)
        logging.debug("are_headers_correct: checking %s:%s matches %s",hdr, str(returned_hdr), match_pattern )
        if not re.match( match_pattern, returned_hdr ):
            areCorrectFlag = False
    return areCorrectFlag

## os independent path management
#################################

def find_global_data_dir():     ## this is where demo/tutorial data is stored
    ''' global data sit in directory under the top level of the install '''
    ''' so need to back up one level from script location and add /data '''
    ''' in os-independent manner '''
    exec_dir = os.path.dirname( os.path.realpath( __file__ ) ) # works on windows, but unix???
    return os.path.join( os.path.split(exec_dir)[0], "data" )

def find_global_pipeline_dir():     ## this is where result vidjil etc data is stored
    ''' global data sit in directory under the top level of the install '''
    ''' so need to back up one level from script location and add /data '''
    ''' in os-independent manner '''
    exec_dir = os.path.dirname( os.path.realpath( __file__ ) ) # works on windows, but unix???
    return os.path.join( os.path.split(exec_dir)[0], "pipeline_results" )
       
def find_global_defaults_dir():
    ''' global defaults sit in directory under the top level of the install '''
    ''' so need to back up one level from script location and add /defaults '''
    ''' in os-independent manner '''
    exec_dir = os.path.dirname( os.path.realpath( __file__ ) ) # works on windows, but unix???
    return os.path.join( os.path.split(exec_dir)[0], "defaults" )

def fully_qualify_default_path_to(fname):
    default_dir = find_global_defaults_dir()
    return os.path.join( default_dir, fname)    
    
def get_clean_prog_name():
    ''' return just main prog name '''
    ''' needed because windows puts whole path into __file__ '''
    return (os.path.basename(sys.argv[0]))

def build_path_to_dataset_dir( lib, dataset ):
    
    pipeline_dir = find_global_pipeline_dir()
    tmp = os.path.join(pipeline_dir,lib)
    tmp = os.path.join(tmp, dataset)
    if not os.path.isdir(tmp):
        raise ValueError("Can't find path for requested lib & dataset") # idc improve
    return (tmp)
    
def build_analysis_filename( lib, dataset, analysis_ext, *, check_exists = False ):  # idc chain errors
    fname = os.path.join(build_path_to_dataset_dir (lib, dataset ), dataset+"."+analysis_ext )
    if check_exists:
        if not os.path.isfile(fname):
            raise ValueError("Unable to find required analysis file")   # idc improve error
    return fname
    
def build_library_dataset_obj( known_ext = ["vidjil"]):    # this clearly needs to be cleverer with multiple 
                                                    # data sources, cloud etc
                                                    # can also be refactored to remove duplication of data under
                                                    # different keys
                                                    
                                                    ## there's a lurking bug here on changing dir structure in final project version
                                                    
    lib_dset_obj = dict()
    lib_dset_obj["type"] = "vigr library/dataset object"
    libraries = list ()
    all_datasets = list ()
    pipeline_dir = find_global_pipeline_dir()
    lib_dirs =[name for name in os.listdir(pipeline_dir) if os.path.isdir(os.path.join(pipeline_dir, name))]
    
    for libdir in lib_dirs:
        subdir = os.path.join(pipeline_dir, libdir)

        lib = dict()
        lib["lib_name"] = libdir    
        lib["id"] = libdir      # this is the key used to index in vigr_core.js
        
        lib_desc_file = os.path.join(subdir, "description.txt")
        if os.path.isfile(lib_desc_file):
            with open(lib_desc_file) as f:
                lib["desc"] = f.read()
        else:
            lib["desc"]=""
            pass
        
        # now find all subdirs containing data within the library directory
        
        dset_dirs = [name for name in os.listdir(subdir) if os.path.isdir(os.path.join(subdir, name))]
        dataset_list = list()
        dset_id_list = list()
        
        for d in dset_dirs:
            dataset = dict ()
            data_files = list ()
            
            dataset["name"] = d
            dataset["id"] = d       # index used in vigr_core
            dset_dir = os.path.join(subdir, d)
            all_files = [name for name in os.listdir(dset_dir) if os.path.isfile(os.path.join(dset_dir, name))]
                
            # now iterate over files    
            for f in all_files:
                (fname, ext) =  f.rsplit(".",1)

                if ext in known_ext:
                    data_files.append(f)            
                    desc_name = fname+".desc"
                    desc_file = os.path.join(subdir, desc_name)

                    if os.path.isfile(desc_file):
                        with open(desc_file) as f:
                            dataset["desc"] = f.read()
                    else:
                        dataset["desc"] = ""
                dataset["files"] = data_files    
                
            dataset_list.append(dataset)
            dset_id_list.append(dataset["id"])
        
        lib["datasets"]=dataset_list
        lib["dsets"]=dset_id_list
        libraries.append(lib)
        all_datasets.append(dataset_list)
        
        
    lib_dset_obj["libraries"]=libraries    
    #lib_dset_obj["dsets"] = dset_id_list
    lib_dset_obj["datasets"] = all_datasets
    # pprint.pprint (lib_dset_obj)  # debug
    return (lib_dset_obj)

def main():
    print ("vigr_utils: running test code")
    mod_logger.info ("vigr_utils: running test code")
    
    
if ( "__main__"  = __name__ ):
    main()

    
    