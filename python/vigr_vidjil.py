#
# basic vidjil parser, and diversity extraction via vigr_diversity 
#

# standard libraries
import json
import os
import re
## pretty print json to help with development
from pprint import pprint
## maybe bio seqio or async with yield for big files
import vigr_diversity as vdiv
import vigr_utils as vutil


## ideally subclass existing clonotype??
class vidjil:

    _ver_reqd = {"2016b"}   # add new versions here
    _ver_str = "vidjil_json_version"
    def __init__( self, fname ):
        try:
            self.fname = fname
            if not os.path.isfile( self.fname):
                raise ValueError("Can't find file {}".format( self.fname))
            if not os.access( self.fname, os.R_OK):
                raise ValueError("{} exists, but don't have read permission on file".format (self.fname))
            self.read()
            if not self.check_version():
                raise ValueError("{} was written by an unsupported version of vidjil ({})",
                                    "".format (self.fname, self.json[self._ver_str]))
        except:
            raise
            
            
    def read( self ):
        with open (self.fname) as f:    # needs size checks adding
            self.json = json.load(f)
        
        #pprint (self.json)
        self.keys = self.json.keys()    # maybe redundant
        
    def check_version( self ):
        return self.json[self._ver_str] in self._ver_reqd
       
    def extract_clones_by_count ( self ):
        #print ("in extract_clones_by_count")
        popn = dict()   ## temporary to pass into diversity or call on directly
        
        #print ("Have ", len(self.json["clones"]), " clones")
        for cl in self.json["clones"]:
            
            cl_name = cl[ 'name' ]      ## catch KeyError on missing keys
            cl_reads = cl[ 'reads' ]
                        
            if ( 1 < len(cl_reads) ):
                print("ERROR: multiple read values for clone")
                raise ValueError("Composite read value for {}".format(cl_name))
            
            cl_reads = cl_reads[0]
            
            # names are messy - standard???
            #   print (cl_name,":",cl_reads)
                        
            popn[cl_name] = cl_reads
        return ( popn )
    
    ############################### HERE
    
    ###
    def extract_population_by_group_variable ( self, group_by, depth ):
        ''' essentially returns axis label populations depending on the selected germline '''
              
        if not group_by in ("IGV","IGD","IGJ"):
            raise ValueError("extract_population_by_group_variable given unknown segment {}".group_by)
        if not depth in ("core","mid","full"):
            raise ValueError("extract_population_by_group_variable given unknown germline id depth {}".depth)
                
        germlines = dict()
        
        #print ("Have ", len(self.json["clones"]), " clones")
        for cl in self.json["clones"]:
            cl_name = cl[ 'name' ]      ## catch KeyError on missing keys
            if (cl["id"]):
                id = cl["id"]
            else:
                id = cl_name    ## TODO uniqueness check 
            cl_type = cl[ 'germline' ]
            
            cl_germlines = self.extract_base_germlines_from_name_field(cl_name, cl_type, depth)    
            ## could just loop in extract_base and track type there
            
            if (cl_germlines):
                germlines[id]=cl_germlines
         
         # MEMBERS OR LIST??
         ## there is a balancing act here between a simple "population" object and 
         ## an enhanced version that provides additional options for clustering
         ## alternatively we can push back the decision on identifier to early in the calling chain
         ## and ultimately the user - there is an interaction with the format of the (for now) vidjil
         ## output to cover off
         
         
         # check that make_germline_groups is given the desired output
        glines = self.make_germline_groups(germlines, group_by)
        popn = vdiv.Population( glines , vdiv.ObjContains.MEMBERS)
        
        print ("in extract_population_by_group_variable")
        print(popn._pop)
        
        return ( popn )     ## basically want to return a population element that can be passed to MH
   
    def make_germline_groups(self, germlines, group_by):
        return_dict = dict()
        print ("entered make_germline_groups")
        
        for id, germline in germlines.items():  ### SOMETHING ODD HERE
            
            print ("make_germline_groups: ",id,"->",germline)
            
            if group_by in ("IGV","IGD","IGJ"): ## idc make configurable
                                          
                germline_var = germline[group_by]   # this is the selector variables
                                                    # need to make this more flexible
                
                if germline_var in return_dict:
                    ## could use dict of id and germline if wanted   ******** TODO HERE
                    return_dict[germline_var].append(id)
                else:
                    return_dict[germline_var] = list()
                    return_dict[germline_var].append(id)
            else:
                raise ValueError("Unable to build sets based on selected key {}".group_by)
        
        if False:           # debug
            print ("make_germline_groups")
            print ("====================")
            print ( return_dict.keys() )
            for k,v in return_dict.items():
                print (k,":",v)
            
        return (return_dict)
        
   
    def extract_base_germlines_from_name_field(self, vidjil_name, seq_type, depth ):
        ''' break NGS analysis into germline segments '''
        ''' seq_type = IGH or IGL '''
## parsing something like
## "name": "IGHV4-28*02 4/TGCAGACAAAATT/0 IGHD3-10*01 0/TCAACTGC/0 IGHJ6*03",        

        if not depth in ("core","mid","full"):
            raise ValueError("extract_base_germlines given unknown germline id depth {}".depth)

        tmp_germline = dict()   ## this isn't best way to do, but works
        
        ## need to cover off IGH and IGL cases
        igh_core_match = r"(^IGHV\S+)\*\S+\s+\S+\s+(IGHD\S+)\*\S+\s+\S+\s+(IGHJ\S+)\*\S+$"
        igh_mid_match = r"(^IGHV\S+)\s+\S+\s+(IGHD\S+)\s+\S+\s+(IGHJ\S+)$"
        igh_full_match = r"(^IGHV\S+\s+\S+)\s+(IGHD\S+\s+\S+)\s+(IGHJ.+$)"
        
        #igl_match = r"(^IGHV\S*)\s+[^s]*\s+(IGHJ[^s]*)$"
        
        if ( "IGH" == seq_type ):
            core_germlines = re.match(igh_core_match, vidjil_name)
            mid_germlines =  re.match(igh_mid_match, vidjil_name)
            full_germlines = re.match(igh_full_match, vidjil_name)
            germline_mix = dict()
            
            # make a dict with differing levels of tag detail
            # and attach to the segment
            for seg in ("IGV","IGD","IGJ"):
                tmp_germline[seg] = dict()
            
            if core_germlines:   # IGH version
                tmp_germline["IGV"]["core"]= core_germlines.group(1)
                tmp_germline["IGD"]["core"]= core_germlines.group(2)
                tmp_germline["IGJ"]["core"]= core_germlines.group(3)
            if mid_germlines: 
                tmp_germline["IGV"]["mid"]= mid_germlines.group(1)
                tmp_germline["IGD"]["mid"]= mid_germlines.group(2)
                tmp_germline["IGJ"]["mid"]= mid_germlines.group(3)
            if full_germlines: 
                tmp_germline["IGV"]["full"]= full_germlines.group(1)
                tmp_germline["IGD"]["full"]= full_germlines.group(2)
                tmp_germline["IGJ"]["full"]= full_germlines.group(3)
                
            else:
                if False:   # muting during development
                    print("====================")
                    print("Failed to parse following vidjil name")  # think through handling case
                                                                    # likely IGHD related
                    print(vidjil_name)
                    print("====================")
                tmp_germline = False    # make sure isn't added to list
                
        else:
            print ("extract_base_germlines called with group ",group_by)
            raise ValueError("IGL not implemented yet")


        base_germline = dict()
        if tmp_germline:
            for seg in ("IGV","IGD","IGJ"):
                base_germline[seg] = tmp_germline[seg][depth]
        else:
            base_germline = False
        
        return base_germline
        
    def build_mh_map ( self, clones2, depth="core" ):
        ''' iterates over extracted clone variables and '''
        ''' builds map of mh index vs permutated keys '''
    
        mh_map = dict()
        
        # need check for IGH vs IGL
        ## idc make configurable
        for ig_seg in ("IGV","IGD","IGJ"):          
            print("build_mh_map processing ",ig_seg," segment")
            pop1 = self.extract_population_by_group_variable(ig_seg, depth)
            pop2 = clones2.extract_population_by_group_variable(ig_seg, depth)
            
            # so now we have the population against ig_seg
            # need to build each spoke of the MH display object
            # building building union of ig_seq identifiers,
            # clustering on ig_seq and calculating species richness
            # for each cluster and MH overlap
            
            if True:    # debug
                ## testing basic population
                print (pop1.div_index(0) )
                print (pop2.div_index(0) )
                print (pop1.MorisitaHorn(pop2))
        
        # values should be a list of identifiers or identifier dicts
                print ("Pop1")
                print (pop1._pop.keys())
                print (pop1._pop.values())
                print ("Pop2")
                print (pop2._pop.keys())
                print (pop2._pop.values())
                print (type(pop2._pop.values()))
                #for k,v in pop2._pop.items():
                #    print (k,":",v,v.values())
            
            ## HERE
            set_1 = set(pop1._pop.keys())
            set_2 = set(pop2._pop.keys())
            union = set_1 | set_2

            ## alternative approaches to membership - id or other germline
            print (union)
            ## skip zero length for a given set
        
        
def vigr_mh_map( source1, source2, group_by, desc = "" ):
    ''' package a Morisita-Horn map into json object with adapter(s) and data '''
    
    if False:    # debug
        print("entering vigr_mh_map")   
        print (source1)
        print (source2)
        
    lib_ds_fname_1 = vutil.build_analysis_filename( source1["lib"], source1["ds"], "vidjil")
    lib_ds_fname_2 = vutil.build_analysis_filename( source2["lib"], source2["ds"], "vidjil")
    
    if False:    # debug
        print("extracting from")
        print("set1: ", lib_ds_fname_1)
        print("set2: ", lib_ds_fname_2)
        print("grouping by: ", group_by)
        print("depth: ", depth)
        
    clonotypes1 = vidjil( lib_ds_fname_1)
    clonotypes2 = vidjil( lib_ds_fname_2)

    # are datasets adequately labelled??
    
    # first off, the "spoke" labels should be what is returned from the group selection
    # which isn't being captured locally yet
    
    #    print ("processing clonotypes from .vidjil files")
        
        
    data_arr = list()   # returned values go here
    
    ## extract grouping variables
    
    if group_by.upper() in ("IGH", "IGD", "IGD"):
        # do extract operation
        print("grouping by gene segment germline")
        
        ##colontypes1.extract_base_germlines_from_name_field(self, vidjil_name, seq_type, depth ):
        
        # find the spokes (group variable)
        grp_var = list()
        
        ####### HERE TODO
        
        # iterate over group variable
        for grp in grp_var:
        
            # produce population from each clonotype based on selection variable
            # need to handle no overlap cases from here onwards
            # rely on population being null for no members ****
            
            pop1 = ""   ####### HERE TODO
            pop2 = ""
            
            # calculate species richness (0_D) for each population
            if (pop1):
                sr1 = pop1.div_index(0)
            else:
                sr1 = 0
                
            if (pop2):
                sr2 = pop2.div_index(0)
            else:
                sr2 = 0
        
            # calculate the overlap between the two populations
            if (not pop1) or (not pop2):
                overlap = 0
            else:
                overlap = pop1.MorisitaHorn(pop2)
            
            # remember that have to package data as tuple to insert correctly
            # in structure below (sr[12] = species richness, mhi = overlap (0<=mhi<=1) )
            # adapter = '{spoke:"d[0]",sr1:"d[1]",sr2:"d[2]",mhi:"d[3]"}'
            # myArray.append( ("IGH1", 2.0, 6.5, 0.8) )

            data_arr.append( grp, sr1, sr2, overlap)
        
    else:
        print("WARNING: unable to use ",group_by," as a population selector yet")
        # should drop out sensibly here idc
    
    if True: # mock data to test display and commns routines
        print("WARNING: USING MOCK DATA IN VIGR MH MAP **")
        data_arr.clear
        data_arr.append( ("IGH1", 2.0, 6.5, 0.8) )
        data_arr.append( ("IGH2", 10.0, 5.5, 0.3) )
        data_arr.append( ("IGH3", 7.0, 3, 0.44) )
        data_arr.append( ("IGH4", 12.0, 6, 0.84) )
        data_arr.append( ("IGH5", 6.0, 18, 0.21) )
    
    # produce json object to return
    msg = vdiv.vigrMsg ("MH map", desc)
    msg.add_adapter("spoke=group label, sr[12]=species richness, mhi=overlap", 
                            {"spoke":"d[0]","sr1":"d[1]","sr2":"d[2]","mhi":"d[3]"}, 
                            {"spoke":"", "sr1":"", "sr2":"", "mhi":""} )    # no labels for map
    
    # could extract this from adapters idc
    msg.add_data_provides( ["spoke","radius","overlap","id","y"] )
    msg.add_data_array ( data_arr )
    #msg.add_data_array ( self.diversity_profile( order_start, order_end, step) )
    
    return ( msg.get_json() )
        
    
            
def main():
    print ("Test code for vigr_vidjil.py")
    test_fname = "../data/example.vidjil"       ## not in pipeline
    test2_fname = "../data/example3.vidjil"
    clonotypes1 = vidjil(test_fname)
    clonotypes2 = vidjil(test2_fname)
    clonotypes1.build_mh_map(clonotypes2)
    #pop1 = clonotypes1.extract_population_by_group_variable("IGH")
    #pop2 = clonotypes2.extract_population_by_group_variable("IGH")
    #print(pop1.MorisitaHorn(pop2) )
    
    #clonotypes2 = vidjil(test2_fname)
    #pop = vdiv.Population( clonotypes.extract_clones_by_count(), vdiv.ObjContains.MEMBERS)
    #pop2 = vdiv.Population( clonotypes2.extract_clones_by_count(), vdiv.ObjContains.MEMBERS)
         
    #print(pop.div_index(0))
    #print(pop2.div_index(0))
    #pprint(pop.diversity_profile( -1, 5, 0.5))   ## check returned values in test routines
  
    #print(pop.MorisitaHorn(pop2) )
    
if __name__ == '__main__':
    main()