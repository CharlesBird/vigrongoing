#! /usr/bin/python
#
# (c) Charles Bird, 2016
#
# connect to raw data sources
# initially start with local file resources, but this will also handle cloud
# data locations (to pass to api backend - not to execute analysis in this module)
#

import logging

# AWS
import boto3
 
# local dependencies
import vigr_utils as vutils

mod_logger=logging.getLogger(__name__)


# local dependencies
import vigr_globals as globals

def list_user_bucket(bucket_id):
    try:
        bucket_list = ()
        s3 = boto3.resource('s3')
        bucket = s3.Bucket(bucket_id)
        mod_logger.debug("Listing bucket: ", bucket_id)
        for obj in bucket.objects.all():
            mod_logger.debug(obj.key)
        bucket_list.append(obj.key)
    except:
        raise

def main():
    print ("Running ", vutils.get_clean_prog_name(), " as main")
    
if __name__ == "__main__":
    logging.basicConfig(level=globals.preconfig_logging_level)
    mod_logger = logging.getLogger(__name__)
    mod_logger_console= logging.StreamHandler(sys.stdout)
    mod_logger_console.setLevel(logging.DEBUG)
    main()