#! /usr/bin/python
#
# (c) Charles Bird, 2016
#


class configOpenError(Exception):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super(configOpenError, self).__init__(message)

        # Now for your custom code...
        print ("***********************")
        print ()
        print ("Unable to open following configuration file in global defaults directory:")
        print ("\t",message)
        print ()
        print ("Your VIgR installation maybe corrupt - please check access permissions")
        print ("and reinstall if necessary")
        print ()
        print ("***********************")
        ## raise again to end application
        raise
       

class configParseError(Exception):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super(configParseError, self).__init__(message)

        # Now for your custom code...
        print ("***********************")
        print ()
        print ("Unable to parse following configuration file in global defaults directory:")
        print ("\t",message)
        print ()
        print ("Your VIgR installation maybe corrupt - please reinstall if necessary")
        print ()
        print ("***********************")
        ## raise again to end application
        raise       