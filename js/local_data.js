
// mock data for test & development

var mock_datasets = [
    {"id":"123","desc":"Fake set 1"},
    {"id":"456","desc":"Fake set 2"},
    {"id":"321","desc":"Fake set 3"},
    {"id":"995","desc":"Fake set 4"},
    {"id":"996","desc":"Fake set 5"},
    {"id":"997","desc":"Fake set 6"}
];
     
     // should have dsets as array
var mock_libraries = [
    {"id":"Foo","desc":"Tipton Rabies","dsets":"123,456"},
    {"id":"Bar","desc":"Tipton Ebola","dsets":"123,996"},
    {"id":"Qux","desc":"UCB Camelpox, with extra-long description","dsets":"995,996,997"}
];

// config objects
// for the moment these are sitting in the global namespace

var resizeTimer = 300;  // milliSec; don't respond instantly to resize events to avoid freezing UI

var enumSrcT = {    // extend this as add modules
    FILE: 1,
    API: 2
}

var enumMethodT ={  // supported http methods for use with apiObj
    GET:  1,    // safe
    POST: 2     // potentially modifying 
    // NB don't support delete, will access via command to ctrl server in later version
}

var axis_t  = {
    X: 1,
    Y: 2,
    Z: 3
}

var enumGraphT = {
    NULL: 0,
    XY: 1,
    PI: 2,
    NETWORK: 3,
    PHYLO: 4,
    MH_VENN: 5
}

// push these to config/vigr_graph TODO
var graph_types = {};
graph_types["vigr_grph_sel_0"]= enumGraphT.XY;
graph_types["vigr_grph_sel_1"]= enumGraphT.MH_VENN;
graph_types["vigr_grph_sel_2"]= enumGraphT.PHYLO;

// these can be supplied via control server  // need to add requires TODO
var avail_graphs = [
    {   vigr_grph_t:enumGraphT.XY,
        cls:"vigr_grph_xy",
        id:"Simple XY",
        desc:"Fake set 1",
        icon:"../icons/icon_xy.png",
        requires:["x","y"]
    },
    {vigr_grph_t:enumGraphT.XY,cls:"vigr_grph_xyz",id:"Rotating XYZ",desc:"Fake set 2",icon:"../icons/icon_xy.png"},
    //{vigr_grph_t:enumGraphT.PI,cls:"vigr_grph_pi",id:"321",desc:"Fake set 3"},
    //{vigr_grph_t:enumGraphT.NETWORK,cls:"vigr_grph_net",id:"995",desc:"Fake set 4"},
    {vigr_grph_t:enumGraphT.PHYLO,cls:"vigr_grph_simp_phylo",id:"996",desc:"Fake set 5",icon:"../icons/icon_phylo.png"},
    {   vigr_grph_t:enumGraphT.MH_VENN,
        cls:"vigr_grph_mh_venn",
        id:"996",
        desc:"Fake set 5",
        icon:"../icons/icon_xy.png",
        requires:["spoke","radius","overlap"]
    }
];

var graphIcons = [
    {x: 0, y: 4, width: 1, height: 1},
    {x: 0, y: 3, width: 1, height: 1},
    {x: 0, y: 2, width: 1, height: 1},
    {x: 0, y: 1, width: 1, height: 1},
    {x: 0, y: 0, width: 1, height: 1}
];

var graphPlaceholders = [
    {x: 0, y: 0, width: 1, height: 1},
    {x: 1, y: 0, width: 1, height: 1},
    {x: 2, y: 0, width: 1, height: 1},
    {x: 0, y: 2, width: 1, height: 1},
    {x: 1, y: 2, width: 1, height: 1},
    {x: 2, y: 2, width: 1, height: 1}
];

var graphAreaOptions = {                // these are still valid for jquery sortable
    draggable: {
        handle: '.graphMenu',           //  should pick up from clsname object
    }};


/// data for demo purposes
// but have working api options that can replace these
//////////////////////////

var json_set1 = {x:[0,1,2,3,4,5,6,7,8,9],
                 y:[5,480,250, 100, 330, 410, 475, 25, 85, 220],
                 r:[20 ,  90 ,   50 ,   33 ,   95 ,   12 ,   44 ,    67 ,    21 ,  88 ]
                 };

				 

var dataset = [
                  [ 5,     20 ],
                  [ 480,   90 ],
                  [ 250,   50 ],
                  [ 100,   33 ],
                  [ 330,   95 ],
                  [ 410,   12 ],
                  [ 475,   44 ],
                  [ 25,    67 ],
                  [ 85,    21 ],
                  [ 220,   88 ]
              ];
              
			  
var dataset2 = [
                  [ 5,     120 ],
                  [ -80,   290 ],
                  [ 350,   150 ],
                  [ 400,   333 ],
                  [ 430,   295 ],
                  [ 510,   212 ],
                  [ 175,   44 ],
                  [ 25,    167 ],
                  [ 55,    55 ],
                  [ 520,   888 ],
				  [ 44, 3]
              ];

/*			  
var dataset2 = [
                  [ 0, 5,     120 ],
                  [ 1, 80,   290 ],
                  [ 2, 350,   150 ],
                  [ 3, 400,   333 ],
                  [ 4, 430,   295 ],
                  [ 5, 510,   212 ],
                  [ 6, 175,   44 ],
                  [ 7, 25,    167 ],
                  [ 8, 55,    55 ],
                  [ 9, 520,   288 ],
				  [ 10, 44, 3]
              ];
*/

			  
			  
var data_id = ["sample1", "sample2"];       //***** sets up data to display

var colors = ["blue","red"];
