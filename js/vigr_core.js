//
// This is the basic setup of the Vigr environment
//
// (c) Charles Bird, 2016
//
// TODO: minify release versions
//
// move to prototypes on objects idc


// may be better off passing a dict (or two) into this function

// could add callback function to handle return

this.apiObj = function ( prtcl = "https", host, port = 8000, service, method_t, 
                            callback = null, 
                            graph_class = null ){
    this.prtcl      = prtcl;
    this.host       = escape(host);
    this.port       = port;
    this.service    = escape(service);
    this.method_t   = method_t || enumMethodT.GET;     // enumMethodT.{GET,POST}
    this.callback   = callback || null;                // a callback to be executed on completion
    this.graph_class = graph_class || null;            // a selector, where API attached to graph object

    this.core_url   = null;
    this.rawdata    = null;
    this.isReady    = false;
    this.objStore   = {};           // make sure we have an object, which will persist between functions
    this.apiID      = null;         // this looks like the only way to track these through
                                    // async events so can use handlers to track data load completion
                                    // rather than polling the isReady

    
    this.build_apiID = function(){  // needs in-scope (or global) _api array
        var last_id = _api.length - 1;
        this.apiID = last_id +1; 
        _api.push(this);
        return (this.apiID);
    }
    
    this.build_core_url = function(){
                
        this.core_url = this.prtcl+"://"+this.host
        if ( 80 != this.port ){
            this.core_url += ":"+ this.port ;
        }
        this.core_url += "/"+this.service+"/";
        
        return (this);
    }
    
    /*
    this.attach_arr_data_id = function( arr_data_ids ){     // this is not the right way to do it
                                                    // but doing it the right way with a call chain
                                                    // would take too long at the moment
        this.arr_dataslots = arr_data_ids;
    }
    
    this.attach_arr_adapter_id = function (arr_adapter_ids ){
        this.arr_adapterslots = arr_adapter_ids;
    }
    */    
      
    // crazy calling syntax for storeAPI is needed to get an object 
    // as parameter which can then be modified on return
    //
    // entity_id is trailing identifier if used  
    // it can be multiple path components
    // eg lib_id/dataset_id
    //
    this.call = function( params, entity_id ="", storeAPI = this ){    // check pass value on entity_id idc
        // remember these are async, so can't use data immediately
        // listen for apiComplete, with will contain evt.apiID as index into _api
        //
        // idc add error handling via jqXHR object
               
        if (  null == this.apiID || "undefined" == this.apiID ){   // move into init prototype idc
            storeAPI.apiID = this.build_apiID();
        }
        
        if (  null == this.core_url || "undefined" == this.core_url){   // move into init prototype idc
            this.build_core_url();
        }
        if ( "undefined" == typeof(entity_id) || null == entity_id ){
            var url = this.core_url;
        }else{
            var url = this.core_url + entity_id;
        }
        
        //console.log("apiObj calling ", url)
        //console.log("entity_id ", entity_id)
        
        storeAPI.isReady = false;
                
        if ( enumMethodT.GET == this.method_t ){
            this._get( url, params, storeAPI );
        }else if ( enumMethodT.POST == this.method_t ){
            this._post( url, params, storeAPI  );
        }else{
            console.log("apiObj has unknown http method");  // better error idc
        }
    }
    
    
    this._get = function( url, params, storeAPI ){
        
        var tmp = null;
                
        $.get( url, params , function( tmp ){
                    storeAPI.rawdata = tmp;
                    storeAPI.isReady = true;
                    
                    if ( storeAPI.callback ){
                        storeAPI.callback(storeAPI);
                    }
                    $(document).trigger( {type:"apiComplete", apiID:storeAPI.apiID } );
                });
        return( storeAPI.rawdata);
    }
    
    
    this._post = function( url, params, storeAPI ){
        
        var tmp = null;
        
        $.post( url, params , function( tmp ){
                    storeAPI.rawdata = tmp;
                    storeAPI.isReady = true;
                    
                    // could call callback here or defer to the event handler
                    
                    if ( storeAPI.callback ){
                        storeAPI.callback(storeAPI);
                    }
                    $(document).trigger( {type:"apiComplete", apiID:storeAPI.apiID } );
                });
        return( storeAPI.rawdata);
    }
    
}   ////////////// end apiObj

// callbacks for asynchronous API returns

this.echoJson = function ( api_fn ){
    console.log("echoJson testing call back")
    console.log( api_fn.rawdata )
}

this.processLibDataMsg = function ( api_fn ){   // scope to refactor on representation
                                                // and hold datasets in library idc
     lib_arr = []
     dset_arr = []
     
     if ( api_fn.isReady ){ // stop corruption resulting from direct calls of function on invalid data
         
         //console.log("Entering processLibDataMsg")
         //console.log(api_fn.rawdata)
         //console.log(Object.keys( api_fn.rawdata ))
         
         for ( lib in api_fn.rawdata.libraries ){
         //    lib_arr.push(api_fn.rawdata.libraries[lib])
         //    console.log( "lib ",api_fn.rawdata.libraries[lib] )
         //    console.log( "lib id",api_fn.rawdata.libraries[lib].id)
         //    console.log( "lib desc",api_fn.rawdata.libraries[lib].desc)
         
         // need to build the right dset representation for the array
         
             //console.log( "lib dsets",api_fn.rawdata.libraries[lib].dsets)
             dset_arr.push( api_fn.rawdata.libraries[lib].dsets )   // collate all the dsets
                                                                    // potential for overlap names in samples TODO
         }
         // console.log(" datasets", api_fn.rawdata.datasets )
     
         saveLibrariesToLocalStore( api_fn.rawdata.libraries );     // can refactor this idc
         saveDatasetsToLocalStore( api_fn.rawdata.datasets );
         
         this._all_libraries = fetchLibraries();
         this._all_datasets = fetchDatasets();
         
         if ( false ){       // developing/debug
            console.log("In processLibDataMsg")
            console.log("====================")
            console.log("Libraries")
            console.log(this._all_libraries)
            console.log("Datasets")
            console.log(this._all_datasets)
         }

         // kick the updateLibraryListHandler
         $(document).trigger("updateLibList")
     }
}


// what we should actually do with this function is to pass graphObj to allow
// data to be directly associated

this.processVigrDataPackage = function ( api_fn ){
    
     if (false){
        console.log("in processVigrDataPackage")
        console.log(api_fn)
        console.log("graph selector: ", api_fn.graph_class)
     }
     
     var adapter_list = null;
     var dset         = null
        
     if ( api_fn.isReady){      // make sure any direct calls, as opposed to event callbacks,
                                // don't mess up the system with old data
        adapter_list = []
        dataset_list = []
        var json_obj = api_fn.rawdata;     
        adapters = json_obj.adapters
        for (a in adapters){
            console.log("processVigrDataPackage: adapters")
            console.log(adapters[a])
            var adapter = new AdapterObj ( adapters[a].desc, adapters[a].adpt_dict, adapters[a].labels)
            console.log(adapter.name)
            
            adapter.register();
            adapter.build_adapter_fn();
            adapter_list.push(adapter);
        }
        provides = json_obj.provides;
        data = json_obj.data;
        
        if ( 0 >= data.length){
            console.log("ERROR: No data returned in VIgR data package") // inelegant idc
            throw(Error)
        }
        
        if (false){
            console.log("processVigrDataPackage provides/data")
            console.log(provides)
            console.log(data)
        }
        
        
        var ds = new DatasetObj ( json_obj.desc, json_obj.data, json_obj.provides );
        ds.register();
        dataset_list.push(ds)
            
     }
     
     if (false){
        console.log("leaving processVigrDataPackage")
        console.log(ds)
        console.log(adapter_list)
     }
     
     
     api_fn.objStore["dset"] = ds
     api_fn.objStore["arrAd"] = adapter_list
     
     // struggling to trigger event from element, even 
     // though '.resizeable.'+graph_class selector can change css etc
     // so pass the graph_class info in the event, rather than via 'this'
     $(document).trigger({type:"apiDataReady", apiID:api_fn.apiID, graph_class:api_fn.graph_class })
          
     return ( dataset_list, adapter_list )    
}

this.matchGraphsWithDatasets = function (){     // basically an iterative match
                                                
    var graphs_data_matches = {}
                                                    
    for (g in _graphs){     // _graphs is a Vigr level object not global
        for ( d in _all_datasets ){
            var arr_matches = []
            if ( _graphs[g].isSuitable( _add_datasets[d] )){
                arr_matches.push( _all_datasets[d].id )
            }
            graphs_data_matches[_graphs[g].id] = arr_matches;
        }
    }
    return (graphs_data_matches);   
}

/*  // don't need this with callback in original API definition
// not thought through the data flow on this bit
var handleCompletedAPI = function ( api_fn ){
    console.log("in handleCompletedAPI")
    if (api_fn.callback){
        api_fn.callback(api_fn)
    }else{
        // extract object
        if ( api_fn.isReady){
            
            console.log(api_fn.rawdata) // the .post function has parsed this already
            
            //obj = JSON.parse(api_fn.rawdata)
            console.log(typeof(api_fn.rawdata))
            obj = api_fn.rawdata
            console.log( Object.keys(obj) )
        }else{
            throw(Error)    // shouldn't get event dispatched if data isn't ready
        }
    }
}
*/



  // graphObj
  ///////////
  
this.GraphObj = function( graph_t, hasBrush, isBrushable, clsname ){
    this.graph_t = graph_t || enumGraphT.NULL;      // from enum
                                                    // need to manage graph subtypes as well
                                                    
    this.hasBrush   = hasBrush || false;        // can generate brush events
    this.isBrushable = isBrushable || false;    // can react to brush events                                                
    this.clsname = clsname || "";
        
    this.requires = null;  // fetch this from availGraphs
        
    this.title   = "";
    this.svg     = null;
    // per graph settings eg space for title/labels etc
    this.padX = 20;
    this.padY = 20;
    
    this.width = null;  // cache values for enclosing div
    this.height = null;
        
    // overall scaling - use per dataset options for disparate data
    this.extremaDirty = true;           // supports caching of relatively expensive extrema calculation
    this.data_xmin = null;
    this.data_ymin = null;
    this.data_xmax = null;
    this.data_ymax = null;
    
    this.ScaleX  = d3.scale.linear();   // default to 1:1 mapping
    this.ScaleY  = d3.scale.linear();   // this inverts y-axis by default

    this.attachedDatasets = new Set();  // LEGACY
    this.attachedAdapters = new Set();
    this.DsAdPairs = new Set(); // adapter-dataset pairs represent the correct way to interpret data
                                        // since multiple interpretations are possible
                                        // eg a dataset of [gene_id][gc_ratio][usage] could give rise
                                        // to gene_id vs usage      adapter = (x:"d[0]", y:"d[2]") 
                                        // or gene_id vs gc_ratio   adapter = (x:"d[1]", y:"d[2]")
                                        // or usage vs gc           adapter = (x:"d[2]", y:"d[1]") etc
    this.arrDatasets = {};  // dict of obj references
                            // can encapsulate references to datasets 
                            // scales axes etc in obj/arrays
    this.arrAdapters = {};  // NB adapter maps for different names are not necessarily unique
    //
    this.dsIsActive = {};   // is this ds LEGACY
    this.isDsAdActive = {};  
    
    this.data_adapters_api = null;
    
    // methods
    //////////
    
    
    this.isDatasetSuitable = function ( dataset ){
        // this is slightly crude at the moment, as it is decoupled 
        // from what adapters have been provided - refactor idc
        
        if ( !this.requires ){
            for (g in avail_graphs){
                if ( avail_graphs[g].vigr_graph_t == this.graph_t ){
                    this.requires = avail_graphs[g].requires;
                    break;
                }
            }
        }
        
        if ( this.requires && this.requires.length > 0 ){
            
            for ( req in this.requires ){
                if ( ! ( this.requires[req] in dataset.provides )){ // testing NOT
                    return (false)
                }
            }
        }          
        return (true);  // no requirements for data specified, match all   
    }
    
    this.attachDatasetAdapterPair = function ( dsObj, adaptObj, is_active = true){
               
        // when we do this, attach the necessary adapter at the same time to
        // the graph, then all data access can be mediated correctly
        // potential gotcha around using dataset twice with different adapters
        // so may need to think about the keying in sets
        
        console.log("GraphObj.attachDatasetAdapterPair")
        
        console.log(typeof(dsObj.name))
        console.log(typeof(adaptObj.name))
        console.log((dsObj.id))
        console.log((adaptObj.id))
        
        
        //var pair_name = dsObj.name +":"+ adaptObj.name; // escape / trap delim in names idc
        var pair_name = dsObj.id +":"+ adaptObj.id;
       
        
        console.log("GraphObj.attachDatasetAdapterPair: ", pair_name)
        
        this.attachDataset(dsObj, is_active);
        this.attachAdapter(adaptObj);
        
        if ( ("undefined" == typeof( this.DsAdPairs )) || !this.DsAdPairs.has( pair_name ) ){
            this.DsAdPairs.add( pair_name ); 
            console.log("attached pair: ", pair_name)
        }else{
            console.log("skipped attaching dataset/adapter pair")
        }
        // need to run this in all cases
        this.isDsAdActive[ pair_name ] = is_active;
    }
    
    this.attachAdapter = function ( adObj ){
        
        // error check adObj idc
        //var ad_name = adObj.name;
        var ad_name = adObj.id;
                
        if ( ("undefined" == typeof( this.attachedAdapters )) || !this.attachedAdapters.has( ad_name ) ){
            this.arrAdapters[ ad_name ] = adObj;
            this.attachedAdapters.add( ad_name );  // legacy
        }
    }
    
    this.attachDataset = function ( dsObj, is_active = true ){      // are we using reference or object
               
        //var ds_name = dsObj.name;
        var ds_name = dsObj.id;
                
        if ( ("undefined" == typeof( this.attachedDatasets)) || !this.attachedDatasets.has( ds_name ) ){
            this.arrDatasets[ ds_name ] = dsObj;
            this.attachedDatasets.add( ds_name );  // legacy
        }
        // need to run this in all cases
        this.dsIsActive[ ds_name ] = is_active;
        
    }                        

    this.attach_api = function ( api ){
        
        // console.log("GraphObj.attach_api")
        this.data_adapters_api = api;
        //    $(document).on("apiComplete","", function(e){ handleCompletedAPI( _api[e.apiID]); } );
        
        // make an handler specific to this graph that responds to the data collecting apiDataReady event
        // which can be in a closure if need to preserve scope
        //$(document).on("apiDataReady",this.clsname, function(e){ 
        
        /// not tied to class yet
        // console.log(this.clsname)
        $(document).on("apiDataReady", "", function(e){
            // remember you're in a handler - the 'this' context is different
      
            //var func_cls_name = _extract_vigr_class_name( e.target );
            
            // need to extract graph clsname]
            console.log("In apiDataReady handler")
            console.log("=======================")
            console.log(e.target);
            console.log("graph class: ",e.graph_class);
            console.log(Object.keys(_graphs))
       
            if (false){
                console.log("contents of triggering api")
                console.log(_api[e.apiID].rawdata); 
                console.log(_api[e.apiID].objStore.dset); 
                console.log(_api[e.apiID].objStore.arrAd); 
            }
            dset = _api[e.apiID].objStore.dset; 
            adptr = _api[e.apiID].objStore.arrAd[0]; 
            // actually only want to return one adapter on a per graph base - IDC
            //console.log(_api[e.apiID].objStore.arrAd); 
            //console.log(_api[e.apiID].objStore.arrAd); 
            
            if (true){
                console.log("apiDataReady attaching DsAd pair of ")
                console.log(adptr,":",dset.name)
                console.log(dset);
                console.log(adptr);
            }
            
            _graphs[e.graph_class].attachDatasetAdapterPair( dset, adptr );  
        
            // trigger the redraw
            $(".resizable."+e.graph_class).trigger("evtUpdateGraphs");
        
        });
            
            
        //$(document).on("testEvent", this.clsname, function(){console.log(" graph handler ");} )    
        //$(document).on("apiDataReady","",function(){console.log("here");} );
    }
    
    this.detach_api = function ( api ){
        // need to remove handlers as well as api - this is handled automatically in closeGraphHandler
    }
    


    
    // should we ever remove a dataset from object or just mark inactive??
    // graphs are temporary objects, so why bother?
    
   
    this._find_graph_extrema = function(){
    // iterates over all attached, active datasets/adapter pairs to find global min/max
    // needs recalculation (ie following need to set isExtremaDirty/call ) on 
    // - dataset addition/removal
    // - dataset activation/inactivation
    // - adapter change
    //
    // this also needs to handle non-numeric axes (eg gene id)
    //
        var extrm = null;
        
        if (this.extremaDirty){
            for ( let pr of this.DsAdPairs ){
                if ( this.isDsAdActive[pr] ){
                    
                    var pr_names = pr.split(":");
                    
                    console.log("_find_graph_extrema")
                    console.log(pr_names)
                    
                    var ds = this.arrDatasets[  pr_names[0] ];
                    var ad = this.arrAdapters[  pr_names[1] ];
      
                    console.log(ds)
                    console.log(ad)
                    
                    // then call ds.findAdaptedExtremes(ad)
                    // and test those values against graph extremes
                    
                    if ( extrm ){
                        var ds_extrm = ds.findAdaptedExtremes(ad);
                        
                        // TODO extend to difficult cases
                        // simple numeric case
                        extrm["xmin"] = ( ds_extrm.xmin < extrm.xmin ? ds_extrm.xmin : extrm.xmin );
                        extrm["ymin"] = ( ds_extrm.ymin < extrm.ymin ? ds_extrm.ymin : extrm.ymin );
                        extrm["xmax"] = ( ds_extrm.xmax > extrm.xmax ? ds_extrm.xmax : extrm.xmax );
                        extrm["ymax"] = ( ds_extrm.ymax > extrm.ymax ? ds_extrm.ymax : extrm.ymax );
                        
                    }else{  // 1st run, take values without comparison
                        extrm = ds.findAdaptedExtremes(ad);
                    }
                }
            }
            // this.extremaDirty = false;  // disable for now to force recalc while developing adapters etc
            
            // now we need to examine graph_extremes and check whether either axis has null
            // values from a non-numeric dataset, and set appropriately
            
        }
        return ( extrm );
    }
    
    // syntactic sugar - idc look at function passing for type
    this.build_x_scale = function( type = "linear", start_at_zero = true, end_at_zero = true, scale_fn ){
        // start/end_at_zero set whether to stop range appropriately at data min/max or extend to zero
        
        // just pass through scale_fn idc
        var scl = scale_fn || d3.scale.linear()
        
        this.ScaleX = this._build_scale ( axis_t.X, type, start_at_zero, end_at_zero, scl);
        return (this);      // can chain
    }
    
    this.build_y_scale = function( type = "linear", start_at_zero = true, end_at_zero = true, scale_fn ){
        // start/end_at_zero set whether to stop range appropriately at data min/max or extend to zero
        
        var scl = scale_fn || d3.scale.linear();   // TESTING
                                                 // would need more modification code for d3.scale.log()
                                                 // domain etc
                                                
        
        this.ScaleY = this._build_scale ( axis_t.Y, type, start_at_zero, end_at_zero, scl);
        return (this);
    }
    
    // core function
    // needs override of data extrema
    this._build_scale = function( which_axis, type, start_at_zero, end_at_zero, scale_fn, min_dmn, max_dmn  ){
        
        // need to tidy function parameters idc
        
        var scl = scale_fn || d3.scale.linear();    // need to have way of handling domain limits on log scale TODO
                
        console.assert ( which_axis in axis_t);
        
        // 
        // no predefined methods in d3 to determine type of attached scale
        // web suggestions are either explore interface for functions present on different types
        // or to store a .scale.type variable with object at creation time
        //
        //console.log("check linear"+( typeof(d3.scale.linear())==typeof(scl)) );
        //console.log("check log"+( typeof(d3.scale.log())==typeof(scl)) );
        
        //console.log("_build_scale passed function: "+typeof(scale_fn));
        //console.log(scale_fn);
        //console.log("_build_scale using function: "+typeof(scl));
        //console.log(scl);

        //console.log("_build_scale: "+typeof(min_d)+", "+typeof(max_d));
        
        if ( !this.width || !this.height ){ // check cache populated
            console.log("STUB: hit update cache for div.width/height"); // TODO
        }
        
        var dmn = [];
        var range = [];
                
        var extremes = this._find_graph_extrema();
                                                      
        if ( !extremes ){       // will happen on things like MH plot which have configured, rather
                                // data driven domains; give a non-zero domain for now and let them 
                                // reset appropriately
            extremes = {}
            extremes["xmin"] = 0;
            extremes["xmax"] = 1;
            extremes["ymin"] = 0;
            extremes["ymax"] = 1;
        }
                
        switch ( which_axis ){
            case ( axis_t.X ):
                if ( "undefined" == typeof(min_d)){
                    dmin = extremes.xmin;
                }else{
                    // console.log("overriding domain minimum")
                    dmin = min_dmn;
                }
                if ( "undefined" == typeof(max_d)){
                    dmax = extremes.xmax;
                }else{
                    // console.log("overriding domain maximum")
                    dmax = max_dmn;
                }
                range[0] = this.padX;
                range[1] = this.width - this.padX;
                break;
            case ( axis_t.Y ):
                if ( "undefined" == typeof(min_d)){
                    dmin = extremes.ymin;
                }else{
                  //  console.log("overriding domain minimum")
                    dmin = min_dmn;
                }
                if ( "undefined" == typeof(max_d)){
                    dmax = extremes.ymax;
                }else{
                  //  console.log("overriding domain maximum")
                    dmax = max_dmn;
                }
                range[0] = this.height - this.padY;  // reset y-axis to "usual" direction
                range[1] = this.padY;
                break;
                
                // will need radius, colour and other scales idc
            default:
                console.assert("GraphObj.build_scale: unable to handle axis type supplied = "+which_axis);
                break;
        }
        
        dmn[0] = start_at_zero ? Math.min( 0, dmin) : dmin;
        dmn[1] = end_at_zero   ? Math.max( 0, dmax) : dmax;
        
        scl.domain(dmn);
        scl.range(range);
        scl.nice();         // for sensible tick numbers - is this valid for all scale functions TODO ????
        return (scl);
    }
    
    
    // could have array of update callbacks by graph_t ?? 
    // where should brush_map be constructed and attached ??
    
    this.plot = function (){
        
        switch( this.graph_t){
            case ( enumGraphT.XY ):
                this.plot_xy();
                break;
            case ( enumGraphT.MH_VENN ):
                this.plot_MH_venn();
                break;
            default:
                this.plot_message();
                break;
        }
    }
 
    this.plot_message = function ( msg ){
        
        this.msg = msg || "Unable to plot that graph type yet"
        this.hasBrush = false;
        this.isBrushable = false;
        
        this.svg.append("text")
        .attr("x",100)
        .attr("y",100)
        .attr("text-anchor", "middle")  
        .style("font-size", "10px")
        .classed( cssname.graphTitle, true)
        .text( this.msg );
                       
    }
    
    this.plot_xy = function (){
                
        // fudge colourmap for moment TODO
        // should really link to library/dataseries
        //
        var colormap = ["red","blue","green","black","purple"];
        var clr_cnt  = 0;

        this.add_axis( axis_t.X );
        this.add_axis( axis_t.Y );
        
        this.add_title();       
        
        
        
        // test ********
var _native_adapter = {x:"d[0]",y:"d[1]"};     
var adapter = new AdapterObj ("local native", _native_adapter, { x:"x", y:"y"});
adapter.build_adapter_fn();
                
        for ( let ds_name of this.attachedDatasets ){   // set idiom
            if ( "undefined" == typeof(ds_name)){
                console.log("ABORT: undefined dataset in GraphObj.attachedDatasets");
                throw(error);
            }
            
            if (  !this.dsIsActive[ ds_name ] ){
                continue;
            }else if ( this.arrDatasets[ ds_name ].supportsGraph_t ( this.graph_t) ){
                ds = this.arrDatasets[ ds_name ];
                
                var slctr = ds_name;    // tag data series with ds_name
                
                var xsc = this.ScaleX;  // have to access the function prototype here
                var ysc = this.ScaleY;  // and pass via local variable
                                        
                this.svg.selectAll(slctr)
                       .data( ds.data )
                       .enter()
                       .append("circle")        
                       .classed( slctr, true )
                       .attr("fill", colormap[clr_cnt]) // colour fill iteration; extend idc
                       .attr("cx", function(d){ return   xsc( adapter.fn.x(d) ); })
                       .attr("cy", function(d){ return   ysc( adapter.fn.y(d) ); })
    /* keep these as examples */
        //var x_adptr_str = _native_adapter.x;
        //var y_adptr_str = _native_adapter.y;
                       //.attr("cx", function(d){ return xsc(eval( x_adptr_str) ); })    // eval solution
                       //.attr("cy", function(d){ return ysc(eval( y_adptr_str) ); })    // eval solution
                       //.attr("cx", function(d) {
                       //         return ( xsc(d[0]) ); 
                       //     })
                       //.attr("cy", function(d) {
                       //         return ( ysc(d[1]) );
                       //     })
                       .attr("r", 2);                   // potentially tag points with other id
                
                // update fill colour in round-robin
                clr_cnt = ( ++clr_cnt > colormap.length ? 0 : clr_cnt );
                
                // need to think about axes here - name/range etc
                
            }
            
            this.isBrushable = true;
            
        }   
    }
    
    // needs options idc
    this.add_axis = function ( which_axis_t ){
        
        var ax = null;
        
        switch (which_axis_t){
            case ( axis_t.X ):
                this.xAxis = d3.svg.axis().scale(this.ScaleX).orient("bottom");
                ax = this.xAxis;
                break;
            case ( axis_t.Y ):
                this.yAxis = d3.svg.axis().scale(this.ScaleY).orient("right");  // until transform works
                ax = this.yAxis;
                break;
            case ( axis_t.Z ):
                this.zAxis = d3.svg.axis().scale(this.ScaleY).orient("bottom");
                ax = this.zAxis;
                break;
            default:
                console.log("graphObj.add_axis: unrecognised axis "+which_axis_t);
                break;
        }
        // display the axis ( but eventually flag or handle)
        if (ax){
           
            this.svg.append("g")
                .attr("class", "axis")
				//.attr("transform", "translate(0," + (h - padding) + ")")
                .call(ax);
        }
    }
    
    this.add_title = function ( title ){
        
        if (title){     // can't use foo || bar idiom with escape
            this.title = escape(title); // idc escape
        }else{    
            this.title = this.clsname;
        }
        
        // stuff to do on positioning TODO
        this.svg.append("text")
        .attr("x", this.ScaleX(( this.width))/2 )
        .attr("y", this.ScaleY( this.padY / 2)) // not scaled, so measure from top
        .attr("text-anchor", "middle")  
        .style("font-size", "10px")
        .classed( cssname.graphTitle, true)
        .text( this.title );
        
        return(this);   // chainable function
    }
    
        this.add_brush = function (){
       
        // need to attach before data for tooltips to work ***
       
        // this limits to one brush, but can easily be extend to multiple
        // if needed
        // will need to be data driven (one vs two axis extents +/- function)
        
        if (this.hasBrush){ // could do this outside the function
            
            var slctr = "svg."+this.clsname;
            var cls = "brush "+this.clsname;
            
            // make the brush
            this.brush = d3.svg.brush()
                .x(this.ScaleX)
                .y(this.ScaleY)
                .on("brushstart", _brushstart )
                .on("brush", _brushmove)
                .on("brushend", _brushend)
            
            // attach the brush
             d3.select(slctr).append("g")
                .attr("class", cls)
                .call(this.brush);
        }
    }
        
    
    this.plot_radar = function (){
        
    }
    
    this.plot_MH_venn = function ( ln_len, gr_x, gr_y, lbl_xm, lbl_yo ){
    
        // configuration options
        var ln_len  = ln_len || 800
        var gr_x    = gr_x   || 1000
        var gr_y    = gr_y   || 1000
        var lbl_xm  = lbl_xm || 1.4  // multiplier for label x-position beyond ln_len
        var lbl_yo  = lbl_yo || -20  // additive offset for label y-position relative to spoke end
        
        var colormap = ["red","blue","green","black","purple"];
        var clr_cnt  = 0;

        
        // test data
        // will pull these from attached dataset TODO ****
     
        if (false){
            console.log("plot_MH_venn")
            console.log(this.DsAdPairs)
        }  
            
        for ( let pr of this.DsAdPairs){    // set idiom
            
            var tmplist = pr.split(":")
            ds = tmplist[0]
            ad = tmplist[1]
            
        }   // will need to extend loop for multiple data set examples
        
        
        dset = this.arrDatasets[ds]
        adapter = this.arrAdapters[ad]
        
        if (false){
            console.log("MH plot")
            console.log(dset) 
            console.log(adapter)
        }
        
        // quick fix idc move to bound data solution
        var spoke = []
        for ( d in dset.data){
            spoke.push(dset.data[d][0])
        }
          
        // do some general setup   
        var xsc = this.ScaleX;  // access the function prototype here
        var ysc = this.ScaleY;  // and pass via local variable
        this.isBrushable = true;  
        
        // reset the scale domains
        // the size of this graph is configured, not data-driven
        xsc.domain([ (-1*gr_x), gr_x ]);
        ysc.domain([ (-1*gr_y), gr_y ]);
        
        if ( 0 >= spoke.length ){
            throw(Error);   // better error idc
        }

        var angle_step = (2*Math.PI) / spoke.length; // .length is the number of elements NOT length of axis
        var angle = 0
        var i = 0
    
        // there's still the irritating aspect ratio bug on resize that puts the
        // tranlated populations off the spoke axis idc
    
        // 1st population, radius = species richness (diversity order 0_D)
        this.svg.selectAll(".underlying")
                .data(dset.data)
                .enter()
                .append("circle")
                .attr("cx", function(d, i){return xsc (ln_len * Math.cos(angle+(angle_step * i)));})
                .attr("cy", function(d, i){return ysc (ln_len * Math.sin(angle+(angle_step * i++)));})
                .attr("r", function(d){ return   ( adapter.fn.sr1(d) ); })
                .classed("id_b", true)          // marker for brush selection TODO
                .classed("underlying", true);
    
        // second population
        angle = 0
        i = 0
        
        // in this version use the Morisita-Horn index to scale
        // separation (via D3 transform) of the centres (1-MH)(R+r), where R&r are
        // the respective species richness (0_D) indicies
        
        // TODO examine whether area is a better marker than centre separation
            
            this.svg.selectAll(".overlap")
                .data(dset.data)
                .enter()
                .append("circle")
                .attr("cx", function(d, i){return xsc (ln_len * Math.cos(angle+(angle_step * i)));})
                .attr("cy", function(d, i){return ysc (ln_len * Math.sin(angle+(angle_step * i++)));})
                .attr("transform", function(d,i){
                    return("translate ("+
                        ( -1*(1-adapter.fn.mhi(d))*(adapter.fn.sr1(d)+adapter.fn.sr2(d)))*Math.cos(angle+(angle_step*i)) +
                        "," +
                        (1 - adapter.fn.mhi(d))*(adapter.fn.sr1(d)+adapter.fn.sr2(d))*Math.sin(angle_step * i++) +
                        ")");
                    })
                .attr("r", function(d){ return   ( adapter.fn.sr2(d) ); })
                .classed("id_a",true)   // mark with dataset id or similar for selection brushing TODO
                .classed("overlap", true);

        // put a spoke for each "group_by" class
        // either extract from dataset into new array (quickest)
        // or work out how to define line line object
                
        var lineFn = d3.svg.line() 
            .x(function(d) { return xsc(d.x); })
            .y(function(d) { return ysc(d.y); })
        
        angle = 0;
        for ( sp in spoke){
            
            end_x = ln_len * Math.cos(angle);
            end_y = ln_len * Math.sin(angle);
            line = [{ "x":0, "y":0 }, {"x":end_x, "y": end_y}]; // scaling is done by lineFn
            
            var lineGraph = this.svg.append("path")
                            .attr("d", lineFn(line))
                            .classed("spoke",true);
                            
            // add label to each spoke - layout needs improving & tooltipping idc
            this.svg.append("text")
                .attr("x", function(d){return xsc (end_x * lbl_xm );})
                .attr("y", function(d){return ysc (end_y + lbl_yo );})
                .classed("spokelabel", true)
                .text(spoke[sp]);
            
            angle += angle_step;
        }
    }
    
    this.add_debug_dot = function ( ){   // quick and dirty marker for svg area
        this.svg.append("circle")
            .attr("cx",20)
            .attr("cy",20)
            .attr("r",5)
            .style("border","2px dashed yellow")
            .style("fill","blue");
    }    
    
    
// probably nugatory
    this.setMarginX = function ( x ){ this.MarginX = x;}
    this.setMarginY = function ( y ){ this.MarginY = y;}
    
}   // graphObj

this.AdapterObj = function ( name, adr_map, adr_names){
    // name: short description of mapping (eg gene_id_vs_usage)
    // adr_map: dict in d3 syntax {eg x:"d[0]", y:"d[1]"} for array [ [x0, y0], [x1, y1], ... ]
    // adr_names: dict {eg x:"gene_id", y:"usage"}
    
    this.name     = escape (name) || "";
    this.descn    = adr_names || {};    // column (or whatever) names - should escape idc
    this.map      = adr_map || {};
    this.fn       = null;
    this.id       = null;
    
    // methods
    //////////
    
    this.register = function(){     // put into the global tracker
                                            // with a random (string) id
                                            // idc add to prototype for auto init
       // allows 1000 adapters idc configure
       
       var already_used = true;
      
       var maxval =999;
       var minval =0;
       var uid = minval-1;
       var i = 0;
       
       if (_all_adapters.length > (maxval - minval)){
           throw(Error);    // out of slots idc improve error
       }
       do {
            var tmp_id = "adptr";
            while (uid < minval || uid > maxval){
                uid = parseInt( (maxval+1)*Math.random() );
            }
            tmp_id += uid.toString()
            already_used = tmp_id in _all_adapters;
       } while ( already_used );

       this.id = tmp_id;
       _all_adapters[tmp_id] = this;
       return (tmp_id);
    }
    
    this.build_adapter_fn = function ( ){   // ideally call on init

        this.fn = {};   // overwrite null marking an empty cache
        
        for ( adpt_ele in this.map){
            var str = "return ("+ this.map[adpt_ele] +")";
            this.fn[adpt_ele] = new Function ("d", str);
        }
        return ( this.fn ); 
    }
  
}   // end of AdapterObj
  
  // is it more logical to do load action here or with source??
  // ** api could return labelling as well - column heading forced to common schema
  
  
function DatasetObj ( name, data, provides, hasHeader ){
    this.name = escape(name) || "";     // ugrade to full escape idc
    this.data = data || null;
    // this.dim  = dim  || 0;
    this.provides = provides || null;
    this.hasHeader = hasHeader || false;

    // for now, use the provides array as the mechanism to match with graphs/adapters
    // but scope to be smarter idc
    
    // this only makes sense in conjunction with adapter
    this.supported_graph_T = new Set ( );     // testing value ***** enumGraphT.XY
    
    // methods
    //////////
    
    this.register = function(){     // put into the global tracker
                                            // with a random (string) id
                                            // idc add to prototype for auto init
                                            // idc use DRY utility
       // allows 1000 datasets idc configure
       
       var already_used = true;
      
       var maxval =999;
       var minval =0;
       var uid = minval-1;
       var i = 0;
       
       if (_all_datasets.length > (maxval - minval)){
           throw(Error);    // out of slots idc improve error
       }
       do {
            var tmp_id = "ds";
            while (uid < minval || uid > maxval){
                uid = parseInt( (maxval+1)*Math.random() );
            }
            tmp_id += uid.toString()
            already_used = tmp_id in _all_adapters;
       } while ( already_used );

       this.id = tmp_id;
       _all_adapters[tmp_id] = this;
       return (tmp_id);
    }
    
    
    this.findAdaptedExtremes = function ( adaptObj ){
        
        var extremes = {};
                
        if ( !adaptObj.fn ){        // if empty function cache, populate it
            adaptObj.build_adapter_fn();           
        }
        
        // check that we have an adapter for an axis - if not then may have non-ordinal data
        // and should return null (also need to think about time )
        
        // remember to execute the adapter within a return or function statement
        if ( "undefined" != typeof(adaptObj.fn.x) ){
            extremes["xmin"] = d3.min(this.data, function(d){ return( adaptObj.fn.x(d) );});
            extremes["xmax"] = d3.max(this.data, function(d){ return( adaptObj.fn.x(d) );});
        }
        if ( "undefined" != typeof(adaptObj.fn.y) ){
            extremes["ymin"] = d3.min(this.data, function(d){ return( adaptObj.fn.y(d) );});
            extremes["ymax"] = d3.max(this.data, function(d){ return( adaptObj.fn.y(d) );});
        }
    
        return (extremes);
    }
    
    
    // this will be driven by analysis software idc TODO ****
    this.add_supports_graph_T = function ( graph_t ){
        support_graph_T.add(graph_T);
    }
    
    this.supportsGraph_t = function ( graph_t ){
        console.log("** STUB: DatasetObj.isGraph_tSupported");
        return (true);
        
        //return (this.supported_graph_T.has(graph_t) );
    }
    
    // add_dataset will need to iterate over open graphs and attach new dataset as appropriate TODO
       
}  // end of datasetObj


/// global demo objects
var ds1 = new DatasetObj( "dataset1", dataset, 3 ); // wrong dimension
var ds2 = new DatasetObj( "dataset2", dataset2, 3 );

var _api = [];  // this has to be global for now, because creating APIs outside vigr
                // there must be a better way to pass api identity in event, but works for now
var _graphs = {}; // can't get handlers to work if vigr level
                
var Vigr = (function () {
  
  // dataset library-related variables
  ////////////////////////////////////
  // these should be an object
  var _current_libs          = [];     // names of libs in use, which serve as pseudo-index into localStorage
 
  var current_lib_isdirty   = false;  
  var _in_use_datasets      = new Set();    // use set as will only need once; memory control is maintained
                                            // by _update_datasets_in_use which handles new/delete
  
  // server related defaults
  this.ctrl_svr ={};
  ctrl_svr._local_port          = 9050;         // or whatever
  ctrl_svr._local_addr          = "localhost";
  ctrl_svr._local_protocol      = "http";
  
  var _graph_ucn_set   = new Set ();    // set of on-screen graph Unique Class Names (UCN)
                                        // don't strictly need as could iterate over selectors
                                        // to find free UCN, but this is faster
                                        // ideally have as "singleton" via graphObj
                                        
  //var _graphs = {}; // need something to hold objects for now

  
  // public variables - can look at "protected" model on 
  // https://www.sitepoint.com/modular-design-patterns-in-javascript/ idc
  //
  
  // css element prefixes
  ///////////////////////
  
  this.prefix = {};
  prefix["vigr"] = "vigr_";
  prefix["func"] = prefix.vigr+"fn_";
  prefix["grph"] = prefix.vigr+"grph";
  
  // markers for selection check boxes
  // should just stick with lib prefixes - need to share from vigr_data TODO
  // 
  var _use_lib_prefix 		= "use_lib_";
  var _use_dset_prefix 		= "use_dset_";
  this._use_adptr_prefix    = "use_adptr_";
  
  
  // css classnames
  /////////////////
  
  this.cssname = {};
  cssname["graphTgt"]       = "graphTgt";
  cssname["graphSortList"]  = "graphSortList";  // the <ul> where graphs are added/deleted
  cssname["graphSlots"]     = "graphDiv";       // css class common to all graphs
  cssname["slotBase"]       = "graphSlot_";     // base of unique per graph css class
  cssname["graphStyle"]     = "graphStyle";     // enclosing div for menu and svg
  cssname["graphMenu"]      = "graphMenu";
  cssname["graphSVG"]       = "graphSVG";
  cssname["chartArea"]      = "chartArea";      // the actual svg element inserted by D3
  cssname["graphTitle"]     = "graphTitle";
  cssname["tempFormTgt"]    = "tempFormTgt";    // target for transient forms POSTing data to APIs
  
  // icons
  ////////
  
  this.icons = {};  // refactor - wasting storage idc
  icons.dir = "../icons/";
  icons.graph_close = icons.dir+"close-x.png";
  
  // user selectable options
  //////////////////////////
  
  // idc build dict with description strings to automate setting
  this.user_opt = {};
  user_opt.panelCloseCheck   = true;
  user_opt.confirmGraphClose = false;   
  
  this._browser_cap = {};     // using this to track required functions
 
  this._all_libraries = {};
  this._all_datasets  = {};
  
  this._all_adapters  = {};

  
    
  // browser capability tests
  // ************************
  // do these belong here or in data module
   
  this.setBrowserCap = function (key, bool_val){
        // should we store a string here or boolean???
        _browser_cap[key] = bool_val;    // watchout for string
  }
  
  // can add functionality to save "last used"
  // with checkbox in preferences
  // but some complexity on exit events (window.onbeforeunload)
  // that might not capture all changes -> possibly need heartbeat pattern
  
// library selection panel
//////////////////////////  

    // explicit clear all selections in memory (not data in files etc)
    // can add warning here if needed for unsaved changes
  this.clearSelectionsHandler = function (evt){
      
      var func_cls_name = _extract_vigr_class_name( evt.target );
      
      _current_libs = [];    // make sure that we don't _reference_ current_libs at any point
      _in_use_datasets.clear();
      
      closePanel("."+func_cls_name+".sliding_panel");  // need FQ css class otherwise associated button deleted
      
      // dump the graphs on display
      for ( let g of _graph_ucn_set){
        _delete_graph( cssname.slotBase+g );       
      }
      _graph_ucn_set.clear();        // flush the selector class uids

      $.event.trigger({type: "updateLibList"});
	  return (false);
  };
  
    this.populateLoadDataLibPanel = function ( tgt_panel_class ){
      // generate a scrollable list with a check box, name, dataset names, and (abbreviated) description
      // the dataset names and description should popup or tooltip contents
      
      // the list itself is updated as the contents change
     
    var slctr = "."+tgt_panel_class;        // TODO could go straight to call now
        
    _buildLibSelectList( tgt_panel_class );
    
    }

    var _buildLibSelectList = function ( func_cls_name ){
        
        // content target
        var slctr = "."+func_cls_name+ " > .tool_panel_content";
        $(slctr).empty();
        $(slctr).append("<table class='library_list'></table");
        
        // add style to th for width <td style="width:130px">    TODO ***
        // there's some faff here on picking up width in css
        // also possible move to pure css table
        
		// need to pad datasets from description
		
        $(slctr).append("<thead style='width:100%'><tr>"+
                        "<th class='ds_use' style='width:15%'>Use?</th>"+
                        "<th class='ds_name' style='width:20%'>Name</th>"+
                        "<th class='ds_name' style='width:35%'>Description</th>"+
                        "<th class='ds_name' style='width:30%'>Datasets</th>"+
                        "</tr></thead>");
                                                    
        // style the table body here for the moment 
        $(slctr).append("<tbody style='overflow-y: scroll; overflow-x: hidden;'></tbody");
        
        // build the list dynamically
        // ?? sorting functionality
        
        // this has a BUG on timing between calling this is deterministic fashion during 
        // startup and API call return - 2nd run fixes so ignore for now; idc
        
        for ( lib_id in this._all_libraries){    // pulls all the keys = lib names
            $( slctr + " > tbody" ).append( _build_select_library_table_row( lib_id, func_cls_name ) );
        }    
    };
  
   var _build_select_library_table_row = function (lib_name, func_cls_name ){
        
        // this will interact with the library/dataset representation
        
        var cutoff  = 40;   // this will be dynamic
        var tr_ele = [];
        
        
        // ensure that the id is unique on page hence need distinct prefix
        // and protect against numeric lib name (id must start with alpha in html)
        var chk_id = _use_lib_prefix+lib_name;
        
        tr_ele.push("<tr>");
        tr_ele.push("<td class='ds_use ");
        tr_ele.push(func_cls_name);
        tr_ele.push("'>");
        tr_ele.push("<input type='checkbox' id='"+chk_id+"'");
        
        // check if lib_name used in current_libs to precheck boxes
        if ( _current_libs.indexOf( lib_name ) > -1 ){
            tr_ele.push(" checked");
        }else{
            tr_ele.push(" unchecked");  // debug check
        }
        tr_ele.push(" onchange='Vigr.markPanelDirty(this)' ></td>");
        
        // name
        tr_ele.push("<td>"+lib_name+"</td>");
        
        // description, with length sensitive popup - interaction with html elements/styling
        // how much detail should we get into dataset popups??
        var lib_desc = this._all_libraries[lib_name].desc;
        
        // do something on length here idc TODO
        if ( lib_desc.length > cutoff){
            tr_ele.push("<td>" + lib_desc + "</td>");    // TODO
        }else{
            tr_ele.push("<td>" + lib_desc + "</td>");
        }
        
// is string or array better here
// suspect array, but then need supporting function to generate string of IDs for display
// need to error check - probably easiest in JSON parse sanitise function in load function
        var lib_dsets = this._all_libraries[lib_name].dsets;
        // do something on length here
        if ( lib_dsets.length > cutoff){
            tr_ele.push("<td>" + lib_dsets + "</td>");    // TODO
        }else{
            tr_ele.push("<td>" + lib_dsets + "</td>");
        }
        
        // close out row
        tr_ele.push("</tr>");
            
        return(tr_ele.join(""));
   }

   
    this.updateLibraryListHandler = function (evt){
    // should pick this up from message or registration TODO
    // think about how to refactor this, but working for now
   
        _buildLibSelectList( "vigr_fn_load_lib");
        
        if ( ( 0 == _current_libs.length ) && ( 0 == _in_use_datasets.size ) ){
            inactivateTool( ".vigr_fn_clear_lib");
        }else{
            activateTool( ".vigr_fn_clear_lib");
        }
		return (false);
    }
        
    var _buildDsetSelectList = function ( slctr ){
          
        // delete any prior contents
        $(slctr).empty();
        $(slctr).append("<table class='library_list'></table");
        
        // add style to th for width <td style="width:130px">    TODO
        // there's some faff here on picking up width in css
        // also possible move to pure css table
        
        $(slctr).append("<thead style='width:100%'><tr>"+
                        "<th class='ds_use' style='width:15%'>Use?</th>"+
                        "<th class='ds_name' style='width:20%'>Name</th>"+
                        "<th class='ds_name' style='width:30%'>Description</th>"+
                        "<th class='ds_name' style='width:35%'>Graphs Supported</th>"+
                        "</tr></thead>");
                                                    
        // style the table body here for the moment 
        $(slctr).append("<tbody style='overflow-y: scroll; overflow-x: hidden;'></tbody");
        
        // build the list dynamically
        // ?? sorting functionality
        for ( dset_id in this._all_libraries){
            /// should this be library ????? TODO
            $(slctr+" > tbody").append( _build_select_library_table_row( dset_id ) );
        }
  };


   var _build_select_dataset_table_row = function (dset_name){
        
        // this will interact with the library/dataset representation
        
        var cutoff  = 40;   // this will be dynamic
        var tr_ele = "<tr>";
       
        // ensure that the id is unique on page hence need distinct prefix
        // and protect against numeric lib name (id must start with alpha)
        var chk_id = _use_dset_prefix+lib_name;
        
        // check if lib_name used in current_libs to precheck boxes
        
        //  does indexof work on sets?
        if ( _in_use_datasets.indexOf( dset_name ) > -1 ){
            tr_ele += "<td class='ds_use'><input type='checkbox' id='"+chk_id+"' checked";
        }else{
            tr_ele += "<td class='ds_use'><input type='checkbox' id='"+chk_id+"'";
        }
        tr_ele += "onchange='Vigr.markPanelDirty(this)' ></td>";
        
        // name
        tr_ele += "<td>"+dset_name+"</td>";
        
        // description, with length sensitive popup - interaction with html elements/styling
        // how much detail should we get into dataset popups??
        
        var dset_desc = _all_datasets[dset_name].desc;
        
        // do something on length here idc TODO
        if ( dset_desc.length > cutoff){
            tr_ele += "<td>" + dset_desc + "</td>";    // TODO
        }else{
            tr_ele += "<td>" + dset_desc + "</td>";
        }
        
        // need to return attached adapters and pass through capability parser
        // to generate this section
        // for now mark as TODO stub
        tr_ele += "<td>" + "Stub entry TODO" + "</td>";
               
        /*
        var lib_dsets = _all_libraries[lib_name].dsets;
        // do something on length here
        if ( lib_dsets.length > cutoff){
            tr_ele += "<td>" + lib_dsets + "</td>";    // TODO
        }else{
            tr_ele += "<td>" + lib_dsets + "</td>";
        }
        */
        
        // close out line
        tr_ele += "</tr>";
            
        return(tr_ele);
   }
   
  
  // use case for libraries not properly thought through
  // 
  // just doing a load works, but how should this be integrated
  // with the libraries in use and any that have been created
  //
  
  // second function takes selected value and loads (needs to check for isdirty on current)
  // and also triggers activating of remaining tools
  
    this.confirmLibSelectionHandler = function (evt){
        
        var func_cls_name = _extract_vigr_class_name (evt.target);
        // cancelEventBubble(evt); // ? remove
        
        var tmp_lib_list = [];
      
        var slctr = "." +func_cls_name + " > input:checked";
              
        $(slctr).each(function(){
            var lib_name = $(this).attr("id");
        	lib_name = lib_name.replace(_use_lib_prefix,"");
            tmp_lib_list.push(lib_name);
		});
      
        _current_libs = tmp_lib_list;
        update_datasets_in_use();
    
        markPanelClean("."+func_cls_name);
        
        closePanel("."+func_cls_name+".sliding_panel");
        $.event.trigger({type: "updateLibList"});
        
// ideal place to kick off async loads into memory of selected datasets TODO ***88
        
		return (false);     // stop event bubbling
	};
  
  // make this public so we can use from handler module  TODO naming style
    this.update_datasets_in_use = function(){
        // iterate over current_libraries and regenerate set of _in_use_datasets
        
        _in_use_datasets.clear();
        
        for (n in _current_libs){
            
            // the representation of this could change from csv to array
            var lib_dsets = this._all_libraries[ _current_libs[n] ].dsets;
            
            if ( "string" == typeof( lib_dsets ) ){
                // using csv data
                dset_list = lib_dsets.split(",");
                for ( d in dset_list ){
                    _in_use_datasets.add( dset_list[d]);
                }
            }
            if ( "array" == typeof ( lib_dsets ) || "object" == typeof ( lib_dsets ) ){
                for (d in lib_dsets){
                    _in_use_datasets.add( lib_dsets[d]);
                }
            }
        }
    };
  
  // basic layout engine
  // *******************
  // need to add in functionality for handlers
  // more orientation / position functions
  
  // better functionality around graph names/enum TODO
  
  
    this.buildGraphToolbar = function (tgt_div, layout_array){
        
        
        //console.log(layout_array)   // this is just the numbers
                                    // should pickup major and minor options instead
        
        // need to pick up targets from array or options TODO
        // better name??
        var toolBar_base_id = "vigr_grph_sel_"; 
        
        var grph_sel_panel_slctr  = ".graph_chooser_panels";    // take from globals TODO
                
        var i = 0;
          
        // problem with jquery syntax, _ stopped working
        //_.each( layout_array, function (node) {
        $.each( layout_array, function (node) {
          
            // i as id or look up from array (array better for semantics)
            var toolID = toolBar_base_id + (i++);
            
          //  console.log("buildGraphToolbar: "+toolID)
            
                 
// need to do for final version            
            // test dynamic icon background
            // will pick these values up from options array
            // TODO ******
            if ( 2 == i){
                var icon = ('../icons/icon_phylo.png');
            }else{
                var icon = ('../icons/icon_xy.png');
            }
            
            // can push these all into one var once debugging complete TODO
            var tmp_button = _build_graph_toolbar_button(toolID, icon);
            $(tgt_div).append(tmp_button);
                        
            var tmp_panel = _build_graph_chooser_panel(toolID);
            $(grph_sel_panel_slctr).append(tmp_panel);
            
            // will also need to populate the panel with options
            // place outside the loop if move to the more efficient class iteration method
            var tmp_content = _populate_graph_chooser_panel(toolID);
            var slctr = ".vigr_grph_sel_panel_content."+toolID;
            $(slctr).append(tmp_content);
            
       }, this);
    
    };
    
    var _build_graph_toolbar_button = function ( func_cls_name, icon ){
       
       // the onclick is actually trigged on the enclosing div, but
       // we use the presence of the (in)active class on the _button_ element
       // to control display functions, so a button must be present
       // even if not associated with an event handler
       
        var tmp = [];
        tmp.push('<div class="icon ' + func_cls_name + ' "');
        tmp.push(' onclick=');
        tmp.push("\"Vigr.toggleGraphSelectPanel('.");
        tmp.push(func_cls_name);
        tmp.push("')\"");
        tmp.push('>');
        
        // note for the moment we set all buttons to active
        // once capability code is running we can change that to inactive
        // and use dataset/library change events to set appropriately
       
        tmp.push('<button class="active tool_btn ');
        tmp.push(func_cls_name);
        tmp.push('"><img src ="');
        tmp.push(icon);
        tmp.push('" ></button></div>');
              
        return ( tmp.join("") );
    };
    
    var _build_graph_chooser_panel = function ( func_cls_name ){
        
        var tmp = [];
       
        // ?? clone sliding_panel class for graphs?? TODO
        // should add in marker class and use css :not to style padding-bottom
        // sticking to sliding panel as base allows existing functionality to be reused
        // but does leave a superfluous tool_button div at the bottom
        tmp.push('<div class="ui-widget-content ui-corner-all sliding_panel hide ');
        tmp.push(func_cls_name);
        tmp.push('"><div class="vigr_grph_sel_panel_content ');
        tmp.push(func_cls_name);
        tmp.push('"></div>');
                
        // should this be styled differently ?? TODO
        tmp.push('<div class="tool_buttons ');
        tmp.push(func_cls_name);        // testing and debug
        tmp.push('">');
        tmp.push('</div></div>');
        
        return ( tmp.join("") );
    }
    
    var _populate_graph_chooser_panel = function ( func_cls_name ){
        var tmp = [];
        
        // get option array/json using func_class as index
        // then iterate over keys, aligning divs horizontally
        // simple click acts as select, which need to trigger
        // closing of the underlying panel and display of the correct graph
                
        // will need to break out into function to support remote operations

        var panel_graph_types = graph_types[func_cls_name];
       
        // TODO error on lookup fail
        
        // this is not the most efficient way, but works for now idc
        // could drive from selector instead and build all selector panels in one iteration
        
        for ( g in avail_graphs ){
            if ( avail_graphs[g].vigr_grph_t == panel_graph_types ){
                
                tmp.push(_build_graph_chooser_div ( avail_graphs[g]) );
            }
        }
        
        return ( tmp.join("") );
    }
   
   
    // this does most of the grunt work in producing the interface  
    var _build_graph_chooser_div = function ( graph_desc ){
        var tmp = [];
        
        // create a div to hold all the selectable elements (icon and name)
        // and make it clickable and tooltip-able
                
                
                
        // need to sort calling values        
                
                
        tmp.push('<div class="graph_choice ');
        tmp.push(graph_desc.cls);
        tmp.push('" onclick=');
        tmp.push("\"Vigr.newGraph(");
        tmp.push(graph_desc.vigr_grph_t);      
        tmp.push(")\">");
        
        // tooltip TODO
        
        // create a div to hold the icon, which is inside a button
        // without a handler
        // This enables (in)active class detecting functions to continue
        // to work
        //
        // note for the moment we set all buttons to active
        // once capability code is running we can change that to inactive
        // and use dataset/library change events to set appropriately
       
        tmp.push('<div class="icon ' + graph_desc.cls + ' ">');
        // may need to have different button class
        tmp.push('<button class="active tool_btn ');
        tmp.push(graph_desc.cls);
		if ( "undefined" == typeof(graph_desc.icon)){
			// put some placeholder here
		}else{
			tmp.push('"><img src ="');
			tmp.push(graph_desc.icon);
		}
        tmp.push('"></button></div>');
        
        // annotate icon with name and div to allow styling
        tmp.push('<div class="graph_name"><p>');
        tmp.push(graph_desc.id);
        tmp.push('</p></div>');
        
        tmp.push('</div>');
        
        return ( tmp.join("") );
    }

/////// GRAPH DISPLAY STARTS HERE ////////////
//////////////////////////////////////////////       

    // first build an SVG
    // then take graphID and trigger an updateGraph event
    // the updateGraph event can handle the capability matching
    // addition/removal of data, and therefore share with other actions
    // - will need to tag with graph class, so update handler selection
    // can be granular
    
    // this is effectively a constructor - move into graphObj
    
    // we should be sending additional information about the graph here
    // axis names etc

    
    var _find_datasets_for_graph_type = function ( grph_class ){
        
        // will need to get graph_requirements
        // and interrogate associated adapters
        // somewhere there's a naming question here as well
        
        var avail_ds = [];
        //
        console.log("*** STUB: _find_datasets_for_graph_type returning mock");
        avail_ds.push(ds2);
        avail_ds.push(ds1);
       
        return ( avail_ds );
    }
    
    var _fetch_user_selections = function ( graph_t ){
        console.log("STUB: fetch_user_selections")
        // idc refactor - makes more sense to use library as selection
        // but only do computation on selection (assuming speed constraints can be met`)
        
        // fudge for beta to pass through data, given limited library availability
        
        
        return (null);     // array of dataset/adapter pairs
    }
    
    // better name idc
    
    this.newGraph = function ( graph_t ){    
    // graph_t: enumGraphT          
    // return: graphObj
        
        var g_type = graph_t;
        
        // hide all open panels in a state-protecting way
        _close_open_panels();
        
        // TODO need to interpose additional information gathering step here
        // for axes etc

               
        // build the enclosing html panels
        var slot_cls_name = _build_graph_slot_name();
        
        if ( !slot_cls_name ){
            window.alert("All graph slots used up - delete an existing graph to add new visualisation");
            return;      // crude break out - should be error idc
        }

        var wdgt_html = _build_graph_list_element(slot_cls_name);   // change name idc

        $("."+cssname.graphTgt).append(wdgt_html);
        
        // ensure new graph is associated with sortable/resizable actions
        // idc - pick up associated options
        $("."+cssname.graphTgt).sortable("refresh"); 
        $( ".resizable."+slot_cls_name ).resizable({
            stop: (function(){$(this).trigger("evtUpdateGraphs");})
        });
         
         
        // deploy graph menu into div
        slctr = "."+cssname.graphMenu+"."+slot_cls_name;
        $(slctr).append(_build_graph_div_menu(slot_cls_name));

        
        // deploy new svg area into content div
                
        // NOW DO THE GRAPH SVG ACTIVITIES
        //////////////////////////////////
        
        // build graph object
        // probably easiest to pass a dict 
        // could integrate these into new, and maintain the _graphs element as a singleton object variable idc
        
        
        var new_graph = new GraphObj( g_type );    // also want graph subtype TODO (use a dict)
                                                    //pass name in constructor idc
        new_graph.clsname = slot_cls_name;

        _graphs[slot_cls_name] = new_graph;        // update global object array
        
        $.event.trigger("graphAdded", slot_cls_name);  // not listening at the moment     
        
        // the api should be driven by graph type, and possibly user interaction
        // arr_lib_ds_id = _fetch_user_selections( g_type )
        
        // TODO get parameters here ********
        
        switch ( g_type ){
            case (enumGraphT.MH_VENN):
                var get_data_api = new Vigr.apiObj( "http", "localhost", 8000, "morisitaHorn", enumMethodT.POST,
                            Vigr.processVigrDataPackage, slot_cls_name )

                new_graph.attach_api( get_data_api );
                
                new_graph.data_adapters_api.call( 
                       {    "lib1":"foo", "data1":"example",
                            "lib2":"foo", "data2":"test",
                            "group":"IGH",
                            "depth":""
                       })
        
                break;
            case (enumGraphT.XY):   // the demo xy graph
            
            // this has broken with changed approach to adapters/datasets
/*
                var nativeAd = new AdapterObj("native",{x:"d[0]", y:"d[1]"},{x:"x", y:"y"});
                nativeAd.register();
                nativeAd.build_adapter_fn();
        
                var ds = _find_datasets_for_graph_type (g_type);    // this should find ad/ds pairs
                                                            // for the moment, fudge as all native adaptObj
        
                for ( d in ds ){
                    new_graph.attachDataset(ds[d]);
                    new_graph.attachDatasetAdapterPair ( ds[d], nativeAd );
                }
*/                   
                // break;      // FALL THROUGH
            default:
                console.log("Unknown graph type: ", graph_t)
                break;
        }
        
        
        
    
// shouldn't need this    
        //$(".resizable."+slot_cls_name).trigger("evtUpdateGraphs");
        
        console.log("returning from newGraph")
        
        return (new_graph);
    }
  
    
    this.updateGraphHandler = function( evt ){
        
        slot_cls_name = _extract_graph_unique_cls_name(evt.target);
        
        //var svg_slctr = "."+_extract_graph_unique_cls_name(evt.target)+"."+cssname.chartArea;
        var svg_slctr = "svg."+slot_cls_name;
        var div_slctr = "."+slot_cls_name+"."+cssname.graphSVG;
        
        $(div_slctr).empty();   // delete existing svg
        
        // get relevant graphObj
        var graph_handle = _graphs[slot_cls_name];
        
       
        graph_handle.width =$( div_slctr ).width();         // update with new dimensions of div, which are only
        graph_handle.height =$( div_slctr ).height();       // values that change on resize event
        
        graph_handle.svg = _d3_add_svg_to_graph (slot_cls_name);   // this must find enclosing div
                                                                   // so should set width / height in this
        
        graph_handle.build_x_scale();   // these should be delegated to the plot function as well
        graph_handle.build_y_scale();   
               
        graph_handle.plot();    // this should be better named as it contains more than just plotting **
        
        
        // these should depend on the plot function, not newGraph ***
        // move these into graphObj idc             
        // _d3_add_brush_to_graph (graph_handle);      // may need to attach before data
        graph_handle.add_brush();      // may need to attach before data
        
        //graph_handle.add_title();       
        
        return (false); // stop event bubbling
    } 
    
    
    this.updateDataSetHandler = function ( evt ){
        
        console.log("STUB: updateDataSetHandler");
        return (false);
    }
  
    // add axis option TODO
    // symbol class as well
    var _d3_add_axis_to_graph = function ( graphObj, which_axis_t ){
        
        var ax = null;
        
        switch (which_axis_t){
            case ( axis_t.X ):
                graphObj.xAxis = d3.svg.axis().scale(graphObj.ScaleX).orient("bottom");
                ax = graphObj.xAxis;
                break;
            case ( axis_t.Y ):
                graphObj.yAxis = d3.svg.axis().scale(graphObj.ScaleY).orient("right");  // until transform works
                ax = graphObj.yAxis;
                break;
            case ( axis_t.Z ):
                graphObj.zAxis = d3.svg.axis().scale(graphObj.ScaleY).orient("bottom");
                ax = graphObj.zAxis;
                break;
            default:
                console.log("_d3_add_axis_to_graph: unrecognised axis "+which_axis_t);
                break;
        }
        // display the axis ( but eventually flag or handle)
        if (ax){
           
            graphObj.svg.append("g")
                .attr("class", "axis")
				//.attr("transform", "translate(0," + (h - padding) + ")")
                .call(ax);
        }
    }
    
    // need clear brush button
    
    var _d3_add_brush_to_graph = function (graphObj){
       
        // need to attach before data for tooltips to work ***
       
        // this limits to one brush, but can easily be extend to multiple
        // if needed
        // will need to be data driven (one vs two axis extents +/- function)
        
        if (graphObj.hasBrush){ // could do this outside the function
            
            var slctr = "svg."+graphObj.clsname;
            var cls = "brush "+graphObj.clsname;
            
            // make the brush
            graphObj.brush = d3.svg.brush()
                .x(graphObj.ScaleX)
                .y(graphObj.ScaleY)
                .on("brushstart", _brushstart )
                .on("brush", _brushmove)
                .on("brushend", _brushend)
            
            // attach the brush
             d3.select(slctr).append("g")
                .attr("class", cls)
                .call(graphObj.brush);
        }
    }
        
    // brush event handlers    
        
    var _brushstart = function (){
        
        var slot_cls_name = _extract_graph_unique_cls_name(this);
        var slctr = "svg."+ slot_cls_name;
        d3.select(slctr).classed("selecting", true);
 
 // needs to 
 // - clear previous "real" and dynamic brushes on all windows
 // - enable clear brush button in graph menu
 
 // need to get brush from graphObj
 
       // d3.select("svg").call(brush.move, null);    //  should clear existing brush displays, but not working
       //d3.select("svg").call(_graphs[slot_cls_name].brush.move, null);    //  should clear existing brush displays, but not working
 
 /*
        $("."+slot_cls_name).trigger("testEvent");
        $("."+slot_cls_name).trigger("clearDynamicBrushes");
 */
    }

    var _brushmove = function( ) {
    
    /// TODO
    
// only need to fix this for visual reasons
// for the moment the select box provides enough feedback
    
        var slot_cls_name = _extract_graph_unique_cls_name(this);       
        var slctr = "svg."+ slot_cls_name;
        var e = d3.event.target.extent();
        
        //console.log(e);
        //var d = _graphs[slot_cls_name].arrDatasets[0].data;     // single dataset fudge for testing
        //console.log("_brushmove ");
        //console.log(d[0]);
        //console.log(d3.select(slctr+"> circle"));
        //console.log(d3.select(slctr+"> circle").cx.baseVal.val);
        d3.select(slctr+"> circle").classed("selected", function(d) {    
          //  console.log("->"+d[0]);                                  // need to access object data TODO
          //  console.log(this)
          //  console.log(this.cx);
    //        return e[0][0] <= d[0] && d[0] <= e[1][0]
      //          && e[0][1] <= d[1] && d[1] <= e[1][1];
            return e[0][0] <= d[0] && d[0] <= e[1][0]
                && e[0][1] <= d[1] && d[1] <= e[1][1];
        });
        
    }
    
    var _brushend = function() {
        
        var slctr = "svg."+ _extract_graph_unique_cls_name(this);
        d3.select(slctr).classed("selecting", !d3.event.target.empty()); 
        
        var extnt = d3.event.target.extent();
        $(slctr).trigger({
                type: "propagateBrush",
                bl_x: extnt[0][0],
                bl_y: extnt[0][1],
                tr_x: extnt[1][0],
                tr_y: extnt[1][1]
        });
        
    }

    this.clearAllDynamicBrushesHandler = function(){
        
        var slctr = "svg.dynamicBrush";
        
        d3.select(slctr).remove();
        
        return (false);
    }
    
    // tweak name idc  TODO need to fix the cross graph BUG introduced with the hasBrush/isBrushable changes
    // as well as managing the mapping across adapters, and interrogating the attached dataset correctly
    // ??? can you get domain and range objects and "invert" scale
    this.brushAllGraphsHandler = function (evt){
        
        var slot_cls_name = _extract_graph_unique_cls_name(evt.target);
   
   console.log("brushAllGraphsHandler");
                
                
        console.log(d3.select("rect.dynamicBrush"));
        d3.select("rect.dynamicBrush").remove();
               
                
        for ( g in _graphs){
            if ( g == slot_cls_name){
                continue;
            }else if ( _graphs[g].isBrushable ){
                var slctr = "svg."+_graphs[g].clsname;
                // check the transformed values
                
                var w = _graphs[g].ScaleX(evt.tr_x) - _graphs[g].ScaleX(evt.bl_x);
                var h = _graphs[g].ScaleY(evt.bl_y) -_graphs[g].ScaleY(evt.tr_y);   // scale inverts
                // use right scale
                console.log(slot_cls_name+": w "+w+" h "+h);
                
                /*
                d3.select(slctr).append("circle")
                    .attr("cx", _graphs[g].ScaleX(evt.bl_x))
                    .attr("cy", _graphs[g].ScaleY(evt.bl_y)) // remember the Y scale inverts
                    .attr("fill","red")
                    .attr("r", 5);
                d3.select(slctr).append("circle")
                    .attr("cx", _graphs[g].ScaleX(evt.tr_x))
                    .attr("cy", _graphs[g].ScaleY(evt.tr_y)) // remember the Y scale inverts
                    .attr("color","red")
                    .attr("r", 5);
                */
                d3.select(slctr).append("rect")
                    .attr("x", _graphs[g].ScaleX(evt.bl_x))
                    .attr("y", _graphs[g].ScaleY(evt.tr_y)) // remember the Y scale inverts
                    .attr("width", w)
                    .attr("height", h)
                    .classed("dynamicBrush", true);
                }
        }
                
        return (false);     // stop event bubbling
    }
    
    
    
    var _d3_add_xy_pts_to_graph = function ( graphObj, datasetObj ){
    /*    
        var slctr = datasetObj.name;
        
        /// need to add the data to graphObj, but is here best
        graphObj.arrDatasets.push(datasetObj);
        
        console.log("_d3_add_xy_pts_to_graph -> "+slctr);
        console.log(datasetObj.data);
        
        graphObj.svg.selectAll("circle")
			   .data( datasetObj.data )
			   .enter()
			   .append("circle")
			   .attr("cx", function(d) {
			   		return (graphObj.ScaleX(d[0]));
			   })
			   .attr("cy", function(d) {
			   		return (graphObj.ScaleY(d[1]));
			   })
			   .attr("r", 2);
    */
    }
   
   
    var _build_graph_list_element = function ( sort_cls_name ){
           
        var tmp = [];

        tmp.push('<div class="ui-widget-content resizable ');
        tmp.push(cssname.graphStyle+" "+sort_cls_name);
        tmp.push('"><div class="');
        tmp.push(cssname.graphMenu+" "+sort_cls_name);
        tmp.push('"></div><div class="');
        tmp.push(cssname.graphSVG+" "+sort_cls_name);
        tmp.push('"></div>');
    
        return ( tmp.join("") );
    }

   
   var _build_graph_div_menu = function ( slot_cls_name ){
       // this is generic - are there graph types that would differ?
       
       var tmp = [];
       
       tmp.push('<div class="graphMenuIcons ');
       tmp.push(slot_cls_name);
       tmp.push('">');       // trying to get position right - bit of faff
   
   // track which is which for testing
   /*
       tmp.push('<p style="font-size: 0.5em">');
       tmp.push(slot_cls_name);
       tmp.push('</p>');
   */
   
       /*
       tmp.push('<img src ="'); // testing two icons
       tmp.push(icons.graph_close);
       tmp.push('">');
       */
       tmp.push('<img class="menuicon closeicon ');
       tmp.push(slot_cls_name);
       tmp.push('" src ="');
       tmp.push(icons.graph_close);
       tmp.push('"></div>');
        
       return (tmp.join(""));
   }
   
   this.closeGraphHandler = function (evt){
       
       var slot_cls_name = _extract_graph_unique_cls_name ( evt.target );
       var del_flag = true;
       var slctr = "." + cssname.graphSlots + "." + slot_cls_name;
        
        // capture existing style then highlight selected graph
        // NB assumes that styling is same on all sides
        // candidate to break out into utility function
        
        if (user_opt.confirmGraphClose){
            var bdr_colour = $(slctr).css("borderBottomColor");
            var bdr_wdth = $(slctr).css("borderBottomWidth");
            var bdr_sty = $(slctr).css("borderBottomStyle");
        
            $(slctr).css("border","3px solid red");     // idc configurable
            del_flag = window.confirm("Close highlighted graph");
            
            // reset style even if deleting to avoid styling empty graph border
            $(slctr).css("border-color", bdr_colour);
            $(slctr).css("border-width", bdr_wdth);
            $(slctr).css("border-style", bdr_sty);
        }
            
        if (del_flag){
            _delete_graph(slot_cls_name);   // this triggers graphClosed event
        }
        return (false); // stop  event bubbling
   }
   
   this._extract_graph_unique_cls_name = function ( dom_ele ){
        
        // has same recursive issue as extract_func_name if 
        // called with wrong type
        
        // TODO bring  _extract code base together - only difference is class prefixes
        
        var mtch = "^"+ cssname.slotBase +"\.*";
        var re = new RegExp (mtch);
                
        var cls = $(dom_ele).attr('class'); // undefined if no classes

        if (cls){
            var class_list = cls.split(" ");
            for ( cl in class_list ){
                var tmp_cls = class_list[cl].match(re);          
                if ( tmp_cls && tmp_cls.length >0 ){
                    return(tmp_cls[0]);
                }
            }
        }else{
            return(_extract_graph_unique_cls_name( $(dom_ele).parent() ));
        }
        
        return (null);
   }
    
// need to think a bit more about how to handle svg stuff
// they're not html currently, but executed d3 statements    
   
    var _d3_add_svg_to_graph = function ( slot_cls_name ){

        var slctr = "."+cssname.graphSVG+"."+slot_cls_name;
        var h = $(slctr).height();
        var w = $(slctr).width();
        
        // to change chart class name TODO
        //var svg_cls = "chartArea "+slot_cls_name;
        var svg_cls = cssname.chartArea+" "+slot_cls_name;
        
        // will need to pick up the enclosing div characteristics
        // and possible have callback for onchange events (eg resize)
        
        // will also need to have handlers for data change and brushing events
        
        // put some sensible sizing here
        
        var svgObj = d3.select( slctr ).append("svg")
                .attr("width", w)
                .attr("height", h)
                .attr("class", svg_cls);
        
        /*  - probably pursue throttled rerender approach 
        // responsive model
        
        var svgObj = d3.select(slctr)
            .append("svg")
            //responsive SVG needs these 2 attributes and no width and height attr
            .attr("preserveAspectRatio", "xMinYMin meet")
            .attr("viewBox", "0 0 600 400")
            //class to make it responsive
            .classed("svg-content-responsive", true); 
        */
        return (svgObj);
    }
    
    
      
   // right, we have a working, addressable svg element that can be added to arbitrary divs in graph area
   //
   // need to think about what attributes a graph will need, by broad class
   // and how to encapsulate
   //
   // broadly xy-type ("orthogonal" values along axes)
   //  - display scale per axis,
   //  - data scale per subset
   //
   // the obvious way to track these is by slot_cls_name
   // slightly wasteful if discard graph and then display 
   // again, but probably reasonable performance hit 
   // (networks and streaming data might be different)
   //
   // this suggests some sort of object...
   //
   
   
   /*
   
   function plot_series(chart_svg_handle, selector_id, api_url, rad) {
// at the moment this naively reads json as x, y duples
// need to add more intelligent parsing
// and also need series property object (color, marker etc)
  
    
 	d3.json(api_url, function(error, json){	// this is async, so need to freeze data_id in call **
			if (error) throw error;

			chart_svg_handle.selectAll(selector_id)
				.data(json)
				.enter()
				.append("circle")
				.attr("cx", function(d) {
				   return xScale(d[0]);
				})
				.attr("cy", function(d) {
					return yScale(d[1]);
				})
				.attr("id", selector_id)
				.attr("r", rad);  
	}); 
}	
*/ 
    
    var _build_graph_slot_name = function (){
        // give unique class name (ucn) leading zeros to allow sensible sorting 
        // allows 1000 graphs which should be plenty!
        
        var graphCls = cssname.slotBase;
        var ucn      = _new_graph_ucn();
        graphCls += ( ucn < 100 ? "0" : "" ); 
        graphCls += ( ucn < 10 ? "0" : "" );
        graphCls += ucn;
        
        return (graphCls);
    }
      
   var _delete_graph_ucn = function ( slot_cls_name ){
       
       // TODO could add defensive check for missing value
       var uid = slot_cls_name.replace( cssname.slotBase, "");
       _graph_ucn_set.delete(uid);
       
   }
         
         
   /// this could be refactored with a reuse list, but works and not a limfac at the moment      
   var _new_graph_ucn = function( minval = 0, maxval = 999 ){
       
       var uid = minval-1;
       var already_used = true;
       
       if ( _graph_ucn_set.size >= (maxval - minval)){
           window.alert("No more graph slots available");       // should throw an error
       }
       do {
            while (uid < minval || uid > maxval){
                uid = parseInt( (maxval+1)*Math.random() );
            }
            already_used = _graph_ucn_set.has(uid);
            
       } while ( already_used );
       _graph_ucn_set.add(uid);
       
       return (uid);
   }
   
   
   // note this does not discard the data in memory
    var _delete_graph = function ( slot_cls_name ){
   
        var graph_obj = $("."+cssname.graphStyle+"."+slot_cls_name);
    
        graph_obj.trigger("graphClosed");       
        graph_obj.remove();                 // removes html and attached handlers
        _delete_graph_ucn(slot_cls_name);
    }
   
   
  // tool & subelement display functions
  // ***********************************
  
    function _inactivate_button(btn_slctr){
        $(btn_slctr).addClass("inactive");
        $(btn_slctr).removeClass("active");
    }; 

    function _activate_button(btn_slctr){
        //console.log( $(btn_slctr) );
        $(btn_slctr).addClass("active");
        $(btn_slctr).removeClass("inactive");
    }; 

    
    
    this.testTool = function ( dom_ele ){
        console.log(dom_ele);
        //if ( $(dom_ele).hasClass("panel_btn")){
        if ( $(dom_ele).hasClass("panel_btn")){
          //  $(dom_ele+'[class*="cancel_btn"]').css("border","2px dashed blue");
            if ( $(dom_ele).is("[class*='panel_btn'")){
                console.log($(dom_ele).attr("class"));
                $(dom_ele).css("border","2px dashed blue");
            }
                
            window.alert("button pressed");
        }
    }
    

    // isDirty functionality
    //
    // will need isDirty enable/disable functions TODO
    // but for moment defined by original html
    
    this.markPanelDirty = function ( dom_ele ){
        _modify_isDirty (dom_ele, true);
    }

    this.markPanelClean = function ( dom_ele ){
        _modify_isDirty (dom_ele, false);
    }

    var _modify_isDirty = function ( dom_ele, flag ){
        
        var panel_slctr = _find_panel_matching_element (dom_ele);

        // the isDirty mechanism is enabled by setting the data-isDirty attribute
        // in the html, so we need to check that we don't inadvertenly switch it on
        if ( $(panel_slctr).attr("data-isDirty") ){
            if (flag){
                $(panel_slctr).attr("data-isDirty","true");
            }else{
                $(panel_slctr).attr("data-isDirty","false");
            }
        }
    }
    
    
    // NB pass a panel selector string, not DOM object here
    this.isPanelDirty = function ( panel_slctr ){
        // [0] needed to get round jQuery lack of native access to attributes
        if ( $(panel_slctr)[0].hasAttribute("data-isDirty") ){
            var isDirty = $(panel_slctr)[0].getAttribute("data-isDirty");
            
            return ( "true" == isDirty.toLowerCase() );
        }else{
            return (false);
        }
    }
    
    // should name more sensibly if this is just being used for inputs
    var _find_panel_matching_element = function (dom_ele){

        var _panel_class_name = _extract_vigr_class_name (dom_ele);
        
        // should we return "" or null or something else here?
        if ( _panel_class_name ) {
            return ("."+_panel_class_name+".sliding_panel");
        }else{
            return ("");
        }
    }

    this._extract_vigr_class_name = function ( dom_ele ){
        
        var mtch = "^"+ prefix.vigr +"\.*";
        var re = new RegExp (mtch);
        
        var cls = $(dom_ele).attr('class'); // undefined if no classes

        if (cls){
            var class_list = cls.split(" ");
            for ( cl in class_list ){
                var tmp_cls = class_list[cl].match(re);          
                if ( tmp_cls && tmp_cls.length >0 ){
                    return(tmp_cls[0]);
                }
            }
        }
        
        // if we get here, no matching class has been found, so 
        // get the parent of the node (which is probably an input in this model)
        // and look for the class in that - recursive
        //
        // NEED TO TRAP WRONG CLASS/NAME TYPE SUPPLIED TO STOP INFINITE LOOP TODO
        // could use counter,  but need closure to track value
        //
        var prnt = $(dom_ele).parent();
        return (_extract_vigr_class_name(prnt));
        
    };
    
    // this handles panel display and reset of isDirty only, not any code to discard dirty data
    this.hidePanelHandler = function (evt){
		var func_cls_name = _extract_vigr_class_name (evt.target);
        var tool_panel_slctr = "." +func_cls_name + ".sliding_panel";
		        
		
        $(tool_panel_slctr).hide("slide");
        $(tool_panel_slctr).addClass("hide");
        $(tool_panel_slctr).removeClass("show");
        markPanelClean("."+func_cls_name);

		return false;	// should stop event bubbling
    };
    
    this.toggleToolOffHandler = function (evt){
        
        cancelEventBubble(evt);     // return false should render this superfluous
       
        var func_cls_name = _extract_vigr_class_name (evt.target);
        var tool_panel_slctr = "."+func_cls_name + ".sliding_panel";

        // force modal save/cancel check on tool panels with significant state
        // (determined by the presence of a true data-isDirty attribute)
        // if we got here without going via the on-panel cancel button
        // for example, clicking the menu button again or opening a graph selection panel
                    
        if ( isPanelDirty(tool_panel_slctr) ){
                // ideally should display name of func_panel or highlight (as with)
                // should probably supply a callback here rather than specify a confirm dialog idc
           var save_changes = true;
           if (user_opt.panelCloseCheck){
                save_changes = window.confirm("Save changes in panel?");
           }

           if (save_changes){
                $("."+func_cls_name).trigger("confirmTool");   
                // the handlers for confirm actions call markPanelClean & close panel
                return (false);	// break out and stop event bubbling
           }
        }
        		
        $("."+func_cls_name).trigger("explicitCancelTool");
           // the hidePanelHandler is associated with this event for all classes
           // and runs markPanelClean which is safe on panels without isDirty tag
        
		return (false);
    };

    var _close_open_panels = function (){
        
        // this must be close to _extract_vigr_class_name
        // look at merging
        
        var slctr = ".sliding_panel.show";
        $(slctr).each(function(){   // should only be one, but in case...
   
            // extract the func or grph class to send toggleToolOff events from
            
            var class_list = ($(this).attr('class')).split(" ");
            
            // will need to be careful about graphs vs grph select objects
            //var close_classes = [_func_prefix, _grph_prefix];
           var close_classes = [ prefix.func,  prefix.grph ];
           
            for (cl in class_list){
                for ( cc in close_classes){
                    var mtch = "^"+close_classes[cc]+"\.*";
                    var re = new RegExp (mtch);
                    var tmp_cls = class_list[cl].match(re);
                   
                    if ( tmp_cls && tmp_cls.length >0 ){
                        $(this).trigger("toggleToolOff");
                    }
                }
            }
		});     // each     
    };
    
    // NB activation is mediated via a button element even if the
    // onclick selection event is attached to something else
    this.activateTool =  function ( func_class ){
        var slctr = func_class+".tool_btn";
        _activate_button( slctr );
    };

    this.inactivateTool =  function ( func_class ){
        var slctr = func_class+".tool_btn";
        _inactivate_button( slctr);
        
        // question about discard of data - probably best to use explicitCancel event idc 
        $(func_class).trigger("explicitCancelTool");  // TODO test if called on closed panel
        
        // closePanel(func_class+".sliding_panel");
    };
    
    this.toggleTool =  function ( func_class ){
      
        var tool_btn_slctr = func_class + ".tool_btn";
        var tool_panel_slctr = func_class + ".sliding_panel";
   
        if ( $(tool_btn_slctr).hasClass("active") ) {
           if ( $(tool_panel_slctr).hasClass("show") ) {
                                
                $(func_class).trigger("toggleToolOff");
                
            }else{
                _close_open_panels();
                showPanel(tool_panel_slctr);
            }
        }
    };

    this.showPanel = function ( tool_panel_slctr ){
        $(tool_panel_slctr).show("slide");
        $(tool_panel_slctr).addClass("show");
        $(tool_panel_slctr).removeClass("hide");
    }
    
    this.closePanel = function ( tool_panel_slctr ){
        $(tool_panel_slctr).hide();
        $(tool_panel_slctr).addClass("hide");
        $(tool_panel_slctr).removeClass("show");
    }
    
    // this is very similar to toggle, but doesn't have to deal with
    // cancel events that manifest through hideTool
    // NB transitioning to event handlers for cancel would make these show/hides
    // simpler to code
    
    this.toggleGraphSelectPanel =  function ( func_class ){
            
        var tool_btn_slctr = func_class + ".tool_btn";
        var tool_panel_slctr = func_class + ".sliding_panel";
   
        if ( $(tool_btn_slctr).hasClass("active") ) {
            if ( $(tool_panel_slctr).hasClass("show") ) {   // double press bug????
                closePanel(tool_panel_slctr);
            }else{
                _close_open_panels();   
                _close_open_panels();    
                showPanel(tool_panel_slctr);
            }
        }
    };
    

    
this.buildGraphArea = function ( gridstack_option_dict){    // this is nugatory
                                                            // apart from marking the draggable handle
                                                            // somehow - should be in the .sortable initialisation
}

// Data source functions
/***********************/

    this.confirmRegisterDataSrcHandler = function ( evt ){
        // what are essential components of src
        // name
        // description
        // protocol
        // address
        // port
        // uri
        // optional components (get/post data)
        // authentication object
        
		console.log("confirmRegisterDataSrc stub");
		window.alert("stub");
		return (false);	// stop event bubbling
    }
  
    
      // debug function to check module loaded OK
      // should change into the if (jQuery) idiom TODO
    this.VIgR = function () {
          return (True);
    }
  
    this.init = function ( clear_existing = false) {
        
        if ( clear_existing){
            clearVigrLocalStore();
        }
        
                
    };
  
  // publish public functions (& variables)
  return (this);

  
})();   // remember to add dependencies - jQuery, D3, gridstack, Registry, Vigr_data
// })(XX || {//console.log("Warning - unable load XX");});

// need to look into http://www.unethicalblogger.com/2008/03/21/javascript-wonks-missing-after-property-id.html
// for errors depending on how dicts are defined

