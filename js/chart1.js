var dataset = [
                  [ 5,     20 ],
                  [ 480,   90 ],
                  [ 250,   50 ],
                  [ 100,   33 ],
                  [ 330,   95 ],
                  [ 410,   12 ],
                  [ 475,   44 ],
                  [ 25,    67 ],
                  [ 85,    21 ],
                  [ 220,   88 ]
              ];

//Create SVG element
var w = 500;
var h = 500;
var padding = 20;


var xScale = d3.scale.linear()
            .domain([0, d3.max(dataset, function(d) { return d[0]; })])
            .range([ padding, w - ( padding *2 ) ]);

var yScale = d3.scale.linear()
			.domain([0, d3.max(dataset, function(d) { return d[1]; })])
            .range([h - padding, padding]);	// flip origin
			 
var rScale = d3.scale.linear()
            .domain([0, d3.max(dataset, function(d) { return d[1]; })])
            .range([1, 100]);
					 
		 
var xAxis = d3.svg.axis()
				.scale(xScale)
				.orient("bottom")
				.ticks(5);						 
		 
var yAxis = d3.svg.axis()
				.scale(yScale)
				.orient("left")
				.ticks(5);	

	
var svg = d3.select("body")
            .append("svg")
			.append("g")
			.attr("class", "scatterplot")
			.attr("width", w)
            .attr("height", h)
			.attr("style", "outline: 1px solid red;");


			
svg.selectAll("circle")
   .data(dataset)
   .enter()
   .append("circle")
   .attr("cx", function(d) {
        return xScale(d[0]);
		})
   .attr("cy", function(d) {
        return yScale(d[1]);
		})
   .attr("r", function(d) {
		return rScale(d[1]/(1+d[0]));	// div by 0 risk
		});

svg.selectAll("text")
   .data(dataset)
   .enter()
   .append("text")
   .text(function(d) {
        return d[0] + "," + d[1];
   })
   .attr("x", function(d) {
        return xScale(d[0]);
   })
   .attr("y", function(d) {
        return yScale(d[1]);
   })
   .attr("font-family", "sans-serif")
   .attr("font-size", "11px")
   .attr("fill", "red");
   
svg.append("g")
	.attr("class", "axis")
	.attr("transform", "translate(0," + (h - padding) + ")")
    .call(xAxis);
	
svg.append("g")
	.attr("class", "axis")
	.attr("transform", "translate(" + padding + ",0)")
    .call(yAxis);
	

	
	