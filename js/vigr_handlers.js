//
//  Vigr event handlers functions
//  here pro tem while structure the rest of the code base
//  
//
//  (c) Charles Bird, 2016
//


var Vigr = (function (Vigr) {

    this.closeOpenPanelsHandler = function (){
        var slctr = ".sliding_panel.show";
        $(slctr).each(function(){
   
            // extract the func or grph class name to pass to hideTool
            // hideTool is state-aware via optional data-isDirty attribute
            
            var class_list = ($(this).attr('class')).split(" ");
            
            // will need to be careful about graphs vs grph select objects
            //var close_classes = ["func","grph"];
            var close_classes = [_func_prefix, _grpg_prefix];
           
            for (cl in class_list){
                for ( cc in close_classes){
                    var mtch = "^"+close_classes[cc]+"\.*";
                    var re = new RegExp (mtch);
                    var tmp_cls = class_list[cl].match(re);
                   
                    if ( tmp_cls && tmp_cls.length >0 ){
                        
                        // ideally push to hideToolHandler
                        
                        hideTool("."+tmp_cls[0]);   // convert to class selector
                    }
                }
            }
		});     
    };
    

    this.cancelPanel = function( dom_ele ){
        
        var func_cls_name = _extract_vigr_class_name(dom_ele);
        $.event.trigger("closePanel", func_cls_name);
    }

    this.handleTestEvent = function (evt, func_cls_name){
        console.log("handleTestEvent called");
        console.log(evt);
        console.log(func_cls_name);
        cancelEventBubble (evt);
        preventEventDefault (evt);
    
        window.alert("Received test event - details on console");
        
        return (false);  // stop bubbling
        
    }
    
    this.cancelEventBubble = function (evt){
        var evtRef = (typeof evt !== "undefined")? evt : event;
        if (evtRef.stopPropagation) {
            evtRef.stopPropagation();
        }
        else {
            evtRef.cancelBubble = true;
        }
    }
    
    this.preventEventDefault = function (evt) {
        var evtRef = (typeof evt !== "undefined")? evt : event;
        if (evtRef.preventDefault) {
            evtRef.preventDefault();
        } else {
            evtRef.returnValue = false;
        }
    }
    
    this.handleConfirmEvent = function (evt, func_cls_name){
        window.alert("Received confirm event in handler");
        console.log("handleTestEvent called");
        console.log(evt);
        console.log(func_cls_name);
    }
    
    
    this.addLocalFileHandler = function (evt){
        console.log("called addLocalFileHandler")
        console.log(evt.target)
        slot_cls_name = _extract_vigr_class_name(evt.target);
        console.log(slot_cls_name)
        
        closePanel("."+slot_cls_name+".sliding_panel")
        
        var files = evt.target.files;
        console.log( files )
        
        return (false)  // stop bubbling
    }
    
    // hardcoded values & style issue TODO
    this.handleFileSelect = function (evt) {
       
        // ??? hide select input button
       
        var files = evt.target.files; // FileList object from HTML5 method

        // files is a FileList of File objects. List some properties.
        var output = [];    // this was defined in the html
                    
        // not picking up the styling for some reason
        for (var i = 0, f; f = files[i]; i++) {
            console.log(f);
            output.push('<div class="filelist_fname">');
            output.push(escape(f.name),'/</div>');
            output.push('<div class="filelist_desc">');
            // needs  onchange handler and better id
            output.push('<input type="text" placeholder="optional description" id="tmp"></div>');
                        
// testing read functionality                        
    //        openData(f);
                              
            /*
            output.push('<li>', escape(f.name), ' (', f.type || 'n/a', ') - ',
                   f.size, ' bytes, last modified: ',
                   f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
                   '<input type="text" placeholder="optional description" id="tmp">',
                   '</li>');
            */
        }
                    
        document.getElementById('local_list').innerHTML = output.join('');
        
        // need to do something to push files back into event data for confirm button
        
        // irritatingly, the security restrictions on the FILE object block path info and clear file on callback******
        
    }

    
    // export public methods
    return (this);

})();    
//})(Vigr || {console.log("Warning - unable load Vigr base module");});
// callout additional dependencies here TODO
// })(Vigr || {console.log("Warning - unable load Vigr base module");});

