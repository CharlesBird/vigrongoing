//
// extension module for vigr data library functions
//
// (c) Charles Bird, 2016
//


var Vigr = (function (){

 // data set management
  // *******************
  // datasets have:
  // a "name"
  // a description
  // 0-* sources of raw data
  // 0-* sources of analytic data
  //
  // where a source has a URI, adapter
  
  // dataset library management engine
  // *********************************
  // this is a simple serialisation of the dataset dict
  // ultimately this can support file, db or api
  // but to start we'll stick with a json in/out
  // to write a local file, we can either go with the local Vigr server
  // or use HTML5 API functions
  
  // these should go into a pseudo class of their own
  
  // should track delimiter in global and remember to sanitise input TODO
	
	  // dataset library-related variables
	  ////////////////////////////////////
	  // these should be an object idc
	var _current_data          = {};     // use this as a dict for the dataset objects in memory
	var _current_lib_names      = {};    // the name of the library if loaded from store
										  // need dict to multiple libraries
	var _current_lib_isdirty   = false;  
        
	var _list_all_library_keys	= {};	// these are prefixed values
	var _list_all_dataset_keys = {};
	  	   
	  // system level library defaults
	  ////////////////////////////////
	var _vigr_lib_prefix      = "vigrlib_";
	var _vigr_lib_delim       = ":";
	var _vigr_lib_list_key    = "vigr_library_names";
	  
	  // this prefix is used to generate namespace for datasets in local storage
	var _vigr_dset_prefix     = "vigrdset_";
	var _vigr_dset_key     	= "vigr_dset_id";
		
		
	// we could use local storage events to capture some of this, 
	// but prefer a more general approach to allow different endpoints
	var	_read_local_lib_dirty = true;
	var	_write_local_lib_dirty = false;
	var	_read_local_dset_dirty = true;
	var	_write_local_dset_dirty = false;
		
	// name clash if used as extension
	this.SetDefaults = function ( 	lib_prefix      = "vigrlib_",
									lib_delim       = ":",
									lib_list_key    = "vigr_library_names",
									dset_prefix     = "vigrdset_",
									dset_key     	= "vigr_dset_id")
	{
		_vigr_lib_prefix      = lib_prefix;
		_vigr_lib_delim       = lib_delim;
		_vigr_lib_list_key    = lib_list_key;
	 
		_vigr_dset_prefix     = dset_prefix;
		_vigr_dset_key     	  = dset_key;
		
		// load existing libraries and data from default source (localStorage for now)
        // should control under save settings TODO
		this._all_libraries = _fetch_libraries();
		this._all_datasets = _fetch_datasets();
	}

	// utility functions to manage namespace in local storage
	// so keys have a (hopefully) unique prefix to avoid collisions
	//
	  var _strip_prefix = function( strng, prefix){
		  var tmp_strng = strng;
		  if ( tmp_strng.startsWith( prefix) ){
			  tmp_strng = tmp_strng.replace( prefix, "") ;
		  }
		  return (tmp_strng.trim());
	  };
	  
	  var _add_prefix = function( strng, prefix){
          try{
              if ("string" != typeof(strng) ){
                  throw("ERROR: _add_prefix called without string to add prefix: "+prefix);
              }
              if ( strng.startsWith( prefix) ){
                  return ( strng.trim() );
              }else{
                  return ( prefix.concat( strng.trim() ));
              }
          }
          catch(err){
              console.log(err);
          }
	  };

	// library and dataset access functions
	///////////////////////////////////////

	// wrappers to make addition of other endpoints easier	
	//

    // need refactor to address scoping
    
    this.fetchLibraries = function (){	
    
        var tmp =  _load_objects_from_local_store(_vigr_lib_prefix);
        this._all_libraries = tmp;
        this._read_local_lib_dirty = false;
        return (tmp)
	}

	this.fetchDatasets = function (){	
        var tmp = _load_objects_from_local_store(_vigr_dset_prefix);
        this._all_datasets =  _load_objects_from_local_store(_vigr_dset_prefix);
        this._read_local_dset_dirty = false;
        return (tmp)
	} 

	var _store_libraries = function (){	
		return ( _save_libraries_to_local_store() );
	} 

	var _store_datasets = function (){	
		return ( _save_datasets_to_local_store() );
	} 

	// HTML5 localStorage access routines
	//

    // could merge core functionality like save
    
    /*
    var _load_libraries_from_local_store = function(){
            
        
		var tmp_lib_arr = [];  
		var tmp_key = "";
		var tmp_name = "";
		
		if ( Boolean( this._browser_cap["local_storage"]) ){
            var len = localStorage.length;
            var cnt = 0;
			for ( var i = 0;  i < len; i++ ) {
				tmp_key = localStorage.key(i);
                
                if ( tmp_key.startsWith(_vigr_lib_prefix)){
					tmp_name = _strip_prefix(tmp_key, _vigr_lib_prefix);
					var tmp_val = localStorage.getItem(tmp_key);
                    
                // can we use reviver in JSON parser to do sanitisation of user-provided values
                // -yes TODO
                    var tmp_arr = JSON.parse(tmp_val);

                    tmp_lib_arr[tmp_name]=tmp_arr;
                    cnt++;
				}
            }
            if (cnt < 1 ){
                console.log("No libraries found in local store");
            }
            _read_local_lib_dirty = false;
		}else{
			  console.log("WARNING : Not able to access local storage to recover library list");
		}
		return (tmp_lib_arr)
	};

    
    // TODO - copy over from above
	var _load_datasets_from_local_store = function(){
		  
		var tmp_dset_arr = [];  	// this is an array of objects (unprefixed)dset_name:dset_json as string
		var tmp_key = "";
		var tmp_name = "";
        var len = localStorage.length;
        var cnt = 0;
		
		if ( Boolean(browser_cap["local_storage"]) ){
			for ( var i = 0; i < len; i++ ) {
				tmp_key = localStorage.key(i);
				if ( tmp_key.startsWith( _vigr_dset_prefix )){
					tmp_name = _strip_prefix(tmp_key, _vigr_dset_prefix);
					tmp_val = localStorage.getItem( tmp_key);
				// ?? stringify
					tmp_lib_arr.push( {dset_name:tmp_name, dset_json:tmp_val });
                    cnt++;
				}
			}
			if (cnt < 1 ){
                console.log("No datasets` found in local store");
            }
            _read_local_dset_dirty = false;
		}else{
			  console.log("WARNING : Not able to access local storage to recover library list");
		}
		return (tmp_dset_arr);
	};
	*/
    
    var _load_objects_from_local_store = function(prefix){
		  
		var tmp_arr = [];   // need to force the "associative" behaviour
                            // but defining as object doesn't deliver right results
		var tmp_key = "";
		var tmp_name = "";
		
		if ( Boolean( this._browser_cap["local_storage"]) ){
            var len = localStorage.length;
            var cnt = 0;
			for ( var i = 0;  i < len; i++ ) {
				tmp_key = localStorage.key(i);
                
                
                if ( tmp_key.startsWith( prefix)){
					tmp_id = String(_strip_prefix(tmp_key, prefix));
					var tmp_val = localStorage.getItem(tmp_key);
                    
                    // console.log("load_objects_from_LS: (",tmp_id,") ", tmp_key,":",tmp_val)
                    
                // can we use reviver in JSON parser to do sanitisation of user-provided values
                // -yes TODO
                    
                    // probably need to do something around validating http(s) endpoints
                    // away from local endpoint
                    // rather than allowing free selection
                    
                    //this problematic with numeric tmp_name
                    // ?? use the dset prefix value after all??
                    tmp_arr[ String(tmp_id) ]= JSON.parse(tmp_val);
                    
                    console.log("load_objects_from_LS: (",tmp_id,") ", tmp_key,":",tmp_val)
                    
                    // console.log( tmp_arr[ String(tmp_id) ] )
                    cnt++;
				}
            }
            if (cnt < 1 ){
                console.log("WARNING: load_objects_from_local_store: No objects matching "+prefix+" found");
            }
            
		}else{
			  console.log("ERROR : Not able to access local storage to recover objects");
              throw(Error)
		}
		return (tmp_arr)
	};

    
    
    // dataset internals
    ////////////////////

    // build flow; create, save to local store as cache, pass to api, +/- add to library
    
    // dataset is clearly an object
	// this is a simple constructor
    
    // note we can;t detect overwrite here - only at insertion into store or library
    // without object pattern and persistent id list 
    // NB need to think about properly escaping user data here TODO
	var _create_dataset = function (taint_id, taint_description, taint_citation="", taint_doi=""){
		var tmp = [];
		
        // do we need _vigr_dset_key or just more to id
       	tmp[_vigr_dset_key] = _escapeString(taint_id);
		tmp.descn = _escapeString(taint_description);
        tmp.cite = _escapeString(taint_citation);
        tmp.doi = _escapeString(taint_doi);
	
		return (tmp);
	};
    
    
    // will need to have an aggregation function for dataset that iterates
    // over all adapters to identify capabilities
	var _create_dataset_handle = function ( id, description, overwrite = false){
        
    };
    
    var _attach_source_to_dataset = function ( dset_id, source_obj, overwrite=false ){
        
    };
    
    var _detach_source_from_dataset = function (dset_id, source_id ){
        
    };
    
    // won't provide amend - create a new object and detach/reattach
    
    // source internals
    ///////////////////
    
    
    // how to represent source_type ?
    //
    // dependency js > 1.8.5
    // var enumSrcType = Object.freeze({"raw":1, "json":2, "tsv":3, "csv":3 })
    
    // need to think carefully about malicious use cases here
    // could datasets (not libraries) share sources -> implies another store
    var _create_source_obj = function ( source_id, source_uri, source_desc, source_type, src_adapter){
        
        var tmp_src = null;
        
        return (tmp_src);  
    };
    
    // adapter internals
    ////////////////////
    
    // these should persist - need to have system/user marker to control amend and delete
    
    // where will system level adapters be sourced - should the user_defined flag be exposed here
    // but will be potentially accessible in local store cache
    // ?? potential sy problem on writebacks -> must check overwrite against source values
    var _create_adapter_obj = function (id, name, description, user_defined=true){
        
    }
    
    var _delete_adapter_obj = function (){
        
    }
    
    
    // library and dataset saving functions
    ///////////////////////////////////////
    
    // syntactic sugar
	this.saveLibrariesToLocalStore = function( array_lib_obj) {
		_save_array_objs_to_local_store( _vigr_lib_prefix, array_lib_obj);
	};
	
    this.saveDatasetsToLocalStore = function( array_dset_obj){      // needs refactor to cope with new structure
            // _save_array_objs_to_local_store( _vigr_dset_prefix, array_dset_obj);
         if ( Boolean( this._browser_cap["local_storage"]) ){
            for ( n in array_dset_obj){
                // console.log("saveDatasetsToLS: ",n )
                // console.log(array_dset_obj[n])
                for (d in array_dset_obj[n]){
                    var tmp_key= _add_prefix( array_dset_obj[n][d]["id"], _vigr_dset_prefix);
                    //console.log(tmp_key)
                    //console.log( array_dset_obj[n][d] );
                    localStorage.setItem(tmp_key, JSON.stringify( array_dset_obj[n][d] ));
                }
            }
		}else{
              console.log("ERROR : Not able to access local storage to write object");
              throw(Error);     // improve idc
		}   
	};
	
    // could use as wrapper to call with endpoint as parameter
    var	_save_array_objs_to_local_store = function ( key_prefix, array_obj){
        
		// can use stringify or toString to handle any functions
		if ( Boolean( this._browser_cap["local_storage"]) ){
            for ( n in array_obj){
                //console.log("_save_arr_local: ",n )
                // console.log(array_obj[n])
                var tmp_key= _add_prefix( array_obj[n]["id"], key_prefix);
                //console.log(tmp_key)
                //console.log( array_obj[n] );
                    
                localStorage.setItem(tmp_key, JSON.stringify( array_obj[n] ));
            }
		}else{
              console.log("ERROR : Not able to access local storage to write object");
              throw(Error);     // improve idc
		}
    }
	
    
    // this creates a single large json
    // more useful in archiving and passing functions
    var _save_monolithic_array_obj_to_local_store = function( key_prefix, array_obj){
		  
        // these should be something else to designate monolithic  
        var obj_name = array_obj["id"];   
		var tmp_key = _add_prefix(lib_name, key_prefix);

		// can use stringify or toString to handle any functions
		if ( Boolean( this._browser_cap["local_storage"]) ){
				localStorage.setItem(tmp_key, JSON.stringify( array_obj ));
		}else{
			  console.log("WARNING : Not able to access local storage to write object");
		}
	};
	
    
	this.clearVigrLocalStore = function(){
        // NB this does not affect in-memory values
               
    	if ( Boolean( this._browser_cap["local_storage"]) ){
            var tmp_key = "";
            var len = localStorage.length
            
            for ( var i = 0; i < len; i++ ) {
				tmp_key = localStorage.key(i);
                              
                console.log("In clearVigrLocalStore - procesing ("+i+") "+tmp_key);
            //
            // should move to in or iterator over defined objects to clear
                if (tmp_key){
                    if ( tmp_key.startsWith( _vigr_lib_prefix ) || tmp_key.startsWith( _vigr_dset_prefix ) ){
                        localStorage.removeItem(tmp_key);
                    }
                }
			}
			_read_local_store_dirty = false;
		}else{
			  console.log("WARNING : Not able to access local storage to delete vigr data");
		}
	};
	
	// these delete both in memory and local store versions
	var _delete_library = function (lib_name){
		  var tmp_lib_name = _add_prefix(lib_name, _vigr_lib_prefix);
		  localStorage.removeItem(tmp_lib_name);
          delete this._all_libraries[lib_name];
	}	

	var _delete_dataset = function (dset_name){
		  var tmp_dset_name = _add_prefix(dset_name, _vigr_dset_prefix);
		  localStorage.removeItem(tmp_dset_name);
          delete this._all_datasets[dset_name];
	}	
	
	  // export public functions
	  return (this);
	  
}).apply(Vigr);	// tight augmentation


