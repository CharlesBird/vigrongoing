//
//
// (c) Charles Bird, 2016
//

// unchanged code from stack

// *****************************************************************************
// this Registry implementation is buggy - the second level path elements are not added to the top level
// dict, but instead become independent values
//
// the real problem is that if the registry is initialised with a simple key:val pair
// it then can't cope with key.subkey:val
// but there is no error checking or handling for this case
//
// we can use it like this for the moment, but will need to add error functionality
// *****************************************************************************


// Registry functions
// break these out into a module
////////////////////////////////

var Registry = function (description=""){
    // this needs to have some form of permanent object store
    // provide a mechanism of tracking uid, and allow some kind
    // of search on both keys and values
    this.key_dict   = {};
    this.dscrptn    = description;
};

// debug function to allow easy check on what registry object is being passed
Registry.prototype.describe = function() {return (this.dscrptn);};


Registry.prototype._setValue = function( dict, key_path, value) {
    // break key_path on . divider
    var key_eles = key_path.split('.');
    //console.log("elements in key path ", key_path);
    //console.log(key_eles);
    var dct = dict;     // fetch current dict  stored in registry
    //console.log("existing registry dict");
    //console.log(dct);
    
    // iterate over all elements in key_path
    for (var i = 0; i < key_eles.length - 1; i++) {
        var ele = key_eles[i];
    //    console.log("testing element from key_path in dict")
    //    console.log(ele);
        
        if (ele in dct) {   // test if ele in existing keys
        //    console.log("matched key: ",ele," - old dct");
        //    console.log(dct);
            dct = dct[ele]; // if yes, then update 
                            // *** THIS IS NOT POINTING TO THE CORRECT LOCATION
        //    console.log("new dct");
        //    console.log(dct);
        } else {
        //    console.log("UNmatched key: ",ele," - adding to dict");
            dct[ele] = {};
            dct = dct[ele];
        //    console.log(dct);
        }
    }
    // following line always runs
    // this should be adding under the full key
    //console.log("Final line existing dict: ",dct);
    //console.log("using key: ",key_eles[key_eles.length-1]);
    // this using the right key, but isn't adding it to the right dict
    // or rather a dict may not exist for it to assign to
    dct[ key_eles[key_eles.length - 1]] = value;
    //console.log("New dict: ",dct);
}

Registry.prototype._getValue = function(dict, key_path) {
     
    var dct = dict;
    key_path = key_path.replace(/\[(\w+)\]/g, '.$1'); // remake classic path
    key_path = key_path.replace(/^\./, '');     // delete leading .
    var key_ele = key_path.split('.');    // iterate over . separated elements
    
    while (key_ele.length) {
        var ele = key_ele.shift(); // traverse key path from leading element
        if (ele in dct) {   // if leading element found, reset dict to look at to this
            // console.log(dct[ele]);
            dct = dct[ele];
            // console.log("found ",ele," > reset head to ",dct);
        } else {
            return;
        }
    }
    return dct;
}

Registry.prototype.set = function(key_path, value) {
    return this._setValue(this.key_dict, key_path, value);
};

Registry.prototype.get = function(key_path) {
    return this._getValue(this.key_dict, key_path);
};

// will need to add serialisation capability here
// registry length query or add iterator functionality
// and test codes


  // console.log(tgt_registry.describe());
  /*
  tgt_registry.set("toolbar","div1");
  tgt_registry.set("toolbar.dscrptn","Div containing tool icons");
  tgt_registry.set("graphTypes","div2");
  
  console.log("** Registry set to ", tgt_registry);
  
  console.log(tgt_registry.get("toolbar"));
  console.log(tgt_registry.get("toolbar.dscrptn"));
  */
  
  /*
  var test_reg = new Registry("testing sub keys from start");
  test_reg.set("toolbar.div","div1");
  // OK this doesn't work either - same problem
  test_reg.set("toolbar.div.background","red");
  test_reg.set("toolbar.dscrptn","Div containing tool icons");
  console.log(test_reg);
  console.log(test_reg.get("toolbar.div"));
  console.log(test_reg.get("toolbar.dscrptn"));
  // OK this doesn't work either - same problem
  //console.log(test_reg.get("toolbar.div.background"));
  */


/* usage example
////////////////

var registry = new Registry();
// NB doesn't like key:val
registry.set('key1.var1', 'value');
registry.get("key1.var1");
registry.set('key1.f1', function(message, suffix){alert(message + suffix);} );
registry.get('key1.f1')('hello', ' world');
    
*/

